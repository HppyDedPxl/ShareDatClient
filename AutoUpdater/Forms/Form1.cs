﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace AutoUpdater
{
    public partial class Form1 : Form
    {
        public Form1(string[] args)
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.None;
            StartPosition = FormStartPosition.CenterScreen;
            // Update shit
            DoDelayed(() =>
            {
                Classes.UpdateHandler uh = new Classes.UpdateHandler(args);

                Process.Start("ShareDat.exe");
                Application.Exit();
            },50);

        }

        /// <summary>
        /// Executes an action after a delay using a worker thread
        /// </summary>
        /// <param name="action"></param>
        /// <param name="ms">time in milliseconds</param>
        public void DoDelayed(Action action, int ms)
        {
            Thread t_worker = new Thread(() =>
            {
                Thread.Sleep(ms);
                action.Invoke();
            });
            t_worker.Start();
        }
    }
}
