﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;

/// <summary>
/// Extremely primitive auto updater class. Replace the link with the Link to the zip file of your version.
/// Main Programm invokes this on Load in MainGUI.cs CheckNewestVersion()
/// </summary>
namespace AutoUpdater.Classes
{ 
    class UpdateHandler
    {

        public UpdateHandler(string[] args)
        {

            // Download newest windows version and extract it.
            using (var client = new WebClient())
            {
                client.DownloadFile("https://allions.net/angularimg/downloads/ShareDat_Win_64_86.zip", "Update.zip");
            }
            ZipArchive zip = ZipFile.Open("Update.zip", ZipArchiveMode.Read);
            foreach (ZipArchiveEntry entry in zip.Entries)
            {
                if (entry.Name != "AutoUpdater.exe") { 
                    entry.ExtractToFile(entry.Name, true);
                }
            }
            zip.Dispose();
            File.Delete("Update.zip");

        }

    }
}
