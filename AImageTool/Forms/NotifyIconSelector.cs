﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AImageTool.Forms
{
    public partial class NotifyIconSelector : Form
    {
        int mposX;
        MainGUI parentUI;
        public NotifyIconSelector(MainGUI parent)
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            parentUI = parent;
            System.Drawing.Rectangle workingRectangle = Screen.PrimaryScreen.WorkingArea;
            this.StartPosition = FormStartPosition.Manual;
            ShowInTaskbar = false;
            this.Location = new Point(MousePosition.X - this.Width, MousePosition.Y - this.Height);
            this.TopMost = true;
            this.BringToFront();
            this.Show();
    
            this.Deactivate += NotifyIconSelector_LostFocus;
            //this.mposX = MousePosition.X;
            //this.Left = Screen.PrimaryScreen.WorkingArea.Width + MousePosition.X - this.Width;
            //this.Top = Screen.PrimaryScreen.WorkingArea.Height + MousePosition.Y - this.Height;

        }

        private void NotifyIconSelector_LostFocus(object sender, EventArgs e)
        {
            parentUI.NotifyIconSelectorLostFocus();
        }

        private void NotifyIconSelector_Load(object sender, EventArgs e)
        {

        }

        private void Button_ConnectionSettings_Click(object sender, EventArgs e)
        {
            Forms.MasterSettings settings = new MasterSettings();
            settings.ChangeTagByName("ConnectionSettings");
            settings.Show();
        }

        private void Button_StartupSettings_Click(object sender, EventArgs e)
        {
            Forms.MasterSettings settings = new MasterSettings();
            settings.ChangeTagByName("StartupSettings");

            settings.Show();
        }

        private void Button_ImageSettings_Click(object sender, EventArgs e)
        {
            Forms.MasterSettings settings = new MasterSettings();
            settings.ChangeTagByName("ImageSettings");
            settings.Show();
        }

        private void Button_OnDoneSettings_Click(object sender, EventArgs e)
        {
            Forms.MasterSettings settings = new MasterSettings();
            settings.ChangeTagByName("CaptureDoneSettings");
            settings.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Button_WebMSettings_Click(object sender, EventArgs e)
        {
            Forms.MasterSettings settings = new MasterSettings();
            settings.ChangeTagByName("WebMSettings");
            settings.Show();
        }

        private void Button_HotkeySettings_Click(object sender, EventArgs e)
        {
            Forms.MasterSettings settings = new MasterSettings();
            settings.ChangeTagByName("HotkeySettings");
            settings.Show();
        }
    }
}
