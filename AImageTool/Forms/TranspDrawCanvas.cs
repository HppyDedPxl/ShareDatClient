﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
namespace AImageTool
{
    internal enum SelectionState
    {
        WaitingForLeftClick = 0,
        Dragging = 1,
        Done = 2
    }

    public delegate void SelectionDone(Rectangle rectCallback);
    public delegate void SelectionAborted();

    public partial class TranspDrawCanvas : Form
    {
        private Point Pivot;
        public Rectangle SelectionRect;

        // true if recording mode is enabled. Basically this means that the selection rectangle transform is retained and not disposed of
        // also the selectionRect will have a different style
        public bool RecordingMode;

        SelectionState m_selectionState;
        SelectionDone rectangleCallback;
        SelectionAborted selectAbortCallback;


        int screenLeft;
        int screenTop;

        public AImageTool.Forms.SelectionRectForm selRectForm;

        public TranspDrawCanvas(SelectionDone ret, SelectionAborted selAbort)
        {
            InitializeComponent();
            ShowInTaskbar = false;
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            screenLeft = SystemInformation.VirtualScreen.Left;
            screenTop = SystemInformation.VirtualScreen.Top;
            int screenWidth = SystemInformation.VirtualScreen.Width;
            int screenHeight = SystemInformation.VirtualScreen.Height;

            this.Size = new System.Drawing.Size(screenWidth, screenHeight);
            this.Location = new System.Drawing.Point(300, screenTop);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            
            this.BackColor = Color.Black;
            //this.TransparencyKey = Color.Black;
            //this.BackgroundImage = Image.FromFile("bg.png");
            Opacity = 0.005;
            m_selectionState = SelectionState.WaitingForLeftClick;
            rectangleCallback = ret;
            selectAbortCallback = selAbort;
            this.MouseDown += OnMouseDown;
            this.MouseMove += OnMouseMove;
            this.MouseUp += OnMouseUp;
            this.BringToFront();
            ShowInTaskbar = false;
            this.KeyUp += new KeyEventHandler(main_KeyUp);

            KeyPreview = true;

            //this.Disposed += TranspDrawCanvas_Disposed;
            
        }

        private void TranspDrawCanvas_Disposed(object sender, EventArgs e)
        {
            if(selRectForm != null)
            {
                selRectForm.Hide();
                selRectForm.Dispose();
            }
        }

        private void main_KeyUp(object sender, KeyEventArgs e)
        {
            Debug.WriteLine(e.KeyCode);
            if (e.KeyCode == Keys.Escape) {
                m_selectionState = SelectionState.Done;
                if (selRectForm != null)
                {
                    selRectForm.Hide();
                    selRectForm.Dispose();
                }
                selRectForm = null;
                selectAbortCallback.Invoke();
            }
        }

        public void AbortSelection()
        {
            m_selectionState = SelectionState.Done;
            if (selRectForm != null)
            {
                selRectForm.Hide();
                selRectForm.Dispose();
            }
            selRectForm = null;
            selectAbortCallback.Invoke();
        }

        protected override void OnPaintBackground(PaintEventArgs e) { /* Ignore */ }

        private void TransparentForm_Load(object sender, EventArgs e) { /* Ignore */ }

        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);
            //if (SelectionRect != null)
            //{
            //    Color c = Color.FromArgb(155, Color.White);
            //    Brush b = new SolidBrush(c);
            //    e.Graphics.FillRectangle(b, SelectionRect);
            //    e.Graphics.DrawRectangle(Pens.White, SelectionRect);

            //}

        }

        protected void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (!(e.Button == MouseButtons.Left)) return;

            if(m_selectionState == SelectionState.WaitingForLeftClick)
            {
                m_selectionState = SelectionState.Dragging;
                Pivot = new Point(e.X, e.Y);
                if (SelectionRect == null)
                    SelectionRect = new Rectangle();

                SelectionRect.X = Pivot.X;
                SelectionRect.Y = Pivot.Y;

                selRectForm = new AImageTool.Forms.SelectionRectForm();
                selRectForm.Parent = this;
                selRectForm.Size = new Size(0, 0);
                selRectForm.Show();

            }
        }

        protected void OnMouseMove(object sender, MouseEventArgs e)
        {
            if(m_selectionState == SelectionState.Dragging)
            {
                int width = e.X - Pivot.X;
                int height = e.Y - Pivot.Y;


                SelectionRect.Location = new Point(Pivot.X + this.Location.X-2, Pivot.Y + this.Location.Y-2);

                // set new rect size   
                // if we go into the negative, invert
                if (width < 0)
                {
                    width = Math.Abs(width);
                    SelectionRect.X = Pivot.X - width + this.Location.X ;
                    SelectionRect.Width = width;
                }
                else
                {
                    SelectionRect.Width = width;
                }
                if(height < 0)
                {
                    height = Math.Abs(height);
                    SelectionRect.Y = Pivot.Y - height + this.Location.Y ;
                    SelectionRect.Height = height  ;
                }
                else
                {
                    SelectionRect.Height = height ;
                }

                this.Invalidate();

                // selection form handling
                //Debug.WriteLine("SelRect: X: {0}, Y: {1}, W: {2}, H: {3}", SelectionRect.Location.X, SelectionRect.Location.Y, SelectionRect.Width, SelectionRect.Height);
                if (selRectForm != null)
                {
                    selRectForm.Location = SelectionRect.Location;// new Point(SelectionRect.Location.X + this.Location.X, SelectionRect.Location.Y+this.Location.Y);
                    selRectForm.Size = SelectionRect.Size;
                    selRectForm.Invalidate();
                }


            }
        }

        protected void OnMouseUp(object sender,MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Right))
            {
                m_selectionState = SelectionState.Done;
                if (selRectForm != null)
                {
                    selRectForm.Hide();
                    selRectForm.Dispose();
                }
                selRectForm = null;
                selectAbortCallback.Invoke();
            }
            else
            {
                if (m_selectionState == SelectionState.Dragging)
                {
                    m_selectionState = SelectionState.Done;
                    rectangleCallback(SelectionRect);
                    if (!RecordingMode)
                    {
                        selRectForm.Hide();
                        selRectForm.Dispose();
                        selRectForm = null;
                    }
                    else
                    {
                        selRectForm.SetRecordingStyle();
                    }

                }
            }
        }

       
    }

 
}
