﻿namespace AImageTool.Forms
{
    partial class MasterSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MasterSettings));
            this.TabControl = new System.Windows.Forms.TabControl();
            this.ConnectionSettings = new System.Windows.Forms.TabPage();
            this.Button_Save = new System.Windows.Forms.Button();
            this.Group_Connection = new System.Windows.Forms.GroupBox();
            this.TextBox_APIKey = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.TextBox_Password = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TextBox_Email = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TextBox_URI = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Button_Cancel = new System.Windows.Forms.Button();
            this.ImageSettings = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Group_Capture = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.DropDown_FullScreenMode = new System.Windows.Forms.ComboBox();
            this.CheckBox_postProcess = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.CaptureDoneSettings = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.OnImageDoneGroup = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Button_SelectSoundPath = new System.Windows.Forms.Button();
            this.TextBox_SoundPath = new System.Windows.Forms.TextBox();
            this.CheckBox_CustomConfirmSound = new System.Windows.Forms.CheckBox();
            this.CheckBox_ToClipboard = new System.Windows.Forms.CheckBox();
            this.CheckBox_AutoUpload = new System.Windows.Forms.CheckBox();
            this.CheckBox_SaveToHDD = new System.Windows.Forms.CheckBox();
            this.Button_SelectPath = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBox_SavePath = new System.Windows.Forms.TextBox();
            this.StartupSettings = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.Group_Startup = new System.Windows.Forms.GroupBox();
            this.CheckBox_StartInTray = new System.Windows.Forms.CheckBox();
            this.CheckBox_StartWithWindows = new System.Windows.Forms.CheckBox();
            this.WebMSettings = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CheckBox_WebM_RecCursor = new System.Windows.Forms.CheckBox();
            this.Label_WebM_CurrentFPS = new System.Windows.Forms.Label();
            this.TrackBar_Webm_Framerate = new System.Windows.Forms.TrackBar();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TrackBar_WebmQuality = new System.Windows.Forms.TrackBar();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.HotkeySettings = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Button_HKey_UploadClip = new System.Windows.Forms.Button();
            this.Label_HKey_UploadClip = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Button_HKey_WebMCaptureStop = new System.Windows.Forms.Button();
            this.Label_HKey_WebMCaptureStop = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Button_HKey_WebMCapture = new System.Windows.Forms.Button();
            this.Label_HKey_WebMCapture = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Button_HKey_AreaCapture = new System.Windows.Forms.Button();
            this.Label_HKey_AreaCapture = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Button_HKey_ScreenCap = new System.Windows.Forms.Button();
            this.Label_HKey_ScreenCap = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.DropDown_NotifiMode = new System.Windows.Forms.ComboBox();
            this.TabControl.SuspendLayout();
            this.ConnectionSettings.SuspendLayout();
            this.Group_Connection.SuspendLayout();
            this.ImageSettings.SuspendLayout();
            this.Group_Capture.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.CaptureDoneSettings.SuspendLayout();
            this.OnImageDoneGroup.SuspendLayout();
            this.StartupSettings.SuspendLayout();
            this.Group_Startup.SuspendLayout();
            this.WebMSettings.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Webm_Framerate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_WebmQuality)).BeginInit();
            this.HotkeySettings.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.ConnectionSettings);
            this.TabControl.Controls.Add(this.ImageSettings);
            this.TabControl.Controls.Add(this.CaptureDoneSettings);
            this.TabControl.Controls.Add(this.StartupSettings);
            this.TabControl.Controls.Add(this.WebMSettings);
            this.TabControl.Controls.Add(this.HotkeySettings);
            this.TabControl.Location = new System.Drawing.Point(0, 0);
            this.TabControl.Multiline = true;
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(375, 296);
            this.TabControl.TabIndex = 1;
            // 
            // ConnectionSettings
            // 
            this.ConnectionSettings.Controls.Add(this.Button_Save);
            this.ConnectionSettings.Controls.Add(this.Group_Connection);
            this.ConnectionSettings.Controls.Add(this.Button_Cancel);
            this.ConnectionSettings.Location = new System.Drawing.Point(4, 40);
            this.ConnectionSettings.Name = "ConnectionSettings";
            this.ConnectionSettings.Padding = new System.Windows.Forms.Padding(3);
            this.ConnectionSettings.Size = new System.Drawing.Size(367, 252);
            this.ConnectionSettings.TabIndex = 0;
            this.ConnectionSettings.Text = "Connection Settings";
            this.ConnectionSettings.UseVisualStyleBackColor = true;
            // 
            // Button_Save
            // 
            this.Button_Save.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Button_Save.Location = new System.Drawing.Point(250, 225);
            this.Button_Save.Name = "Button_Save";
            this.Button_Save.Size = new System.Drawing.Size(102, 23);
            this.Button_Save.TabIndex = 9;
            this.Button_Save.Text = "Save";
            this.Button_Save.UseVisualStyleBackColor = true;
            this.Button_Save.Click += new System.EventHandler(this.Button_Save_Click);
            // 
            // Group_Connection
            // 
            this.Group_Connection.Controls.Add(this.TextBox_APIKey);
            this.Group_Connection.Controls.Add(this.label12);
            this.Group_Connection.Controls.Add(this.TextBox_Password);
            this.Group_Connection.Controls.Add(this.label11);
            this.Group_Connection.Controls.Add(this.TextBox_Email);
            this.Group_Connection.Controls.Add(this.label10);
            this.Group_Connection.Controls.Add(this.TextBox_URI);
            this.Group_Connection.Controls.Add(this.label9);
            this.Group_Connection.Location = new System.Drawing.Point(8, 6);
            this.Group_Connection.Name = "Group_Connection";
            this.Group_Connection.Size = new System.Drawing.Size(350, 178);
            this.Group_Connection.TabIndex = 6;
            this.Group_Connection.TabStop = false;
            this.Group_Connection.Text = "Connection";
            // 
            // TextBox_APIKey
            // 
            this.TextBox_APIKey.Location = new System.Drawing.Point(9, 149);
            this.TextBox_APIKey.Name = "TextBox_APIKey";
            this.TextBox_APIKey.Size = new System.Drawing.Size(335, 20);
            this.TextBox_APIKey.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 133);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "API-Key";
            // 
            // TextBox_Password
            // 
            this.TextBox_Password.Location = new System.Drawing.Point(9, 110);
            this.TextBox_Password.Name = "TextBox_Password";
            this.TextBox_Password.PasswordChar = '*';
            this.TextBox_Password.Size = new System.Drawing.Size(335, 20);
            this.TextBox_Password.TabIndex = 5;
            this.TextBox_Password.TextChanged += new System.EventHandler(this.TextBox_Password_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 94);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Password";
            // 
            // TextBox_Email
            // 
            this.TextBox_Email.Location = new System.Drawing.Point(9, 71);
            this.TextBox_Email.Name = "TextBox_Email";
            this.TextBox_Email.Size = new System.Drawing.Size(335, 20);
            this.TextBox_Email.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "E-Mail";
            // 
            // TextBox_URI
            // 
            this.TextBox_URI.Location = new System.Drawing.Point(9, 32);
            this.TextBox_URI.Name = "TextBox_URI";
            this.TextBox_URI.Size = new System.Drawing.Size(335, 20);
            this.TextBox_URI.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "URI";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Button_Cancel.Location = new System.Drawing.Point(17, 225);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(96, 23);
            this.Button_Cancel.TabIndex = 8;
            this.Button_Cancel.Text = "Cancel";
            this.Button_Cancel.UseVisualStyleBackColor = true;
            this.Button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // ImageSettings
            // 
            this.ImageSettings.Controls.Add(this.button2);
            this.ImageSettings.Controls.Add(this.button1);
            this.ImageSettings.Controls.Add(this.Group_Capture);
            this.ImageSettings.Controls.Add(this.groupBox1);
            this.ImageSettings.Location = new System.Drawing.Point(4, 40);
            this.ImageSettings.Name = "ImageSettings";
            this.ImageSettings.Padding = new System.Windows.Forms.Padding(3);
            this.ImageSettings.Size = new System.Drawing.Size(367, 252);
            this.ImageSettings.TabIndex = 1;
            this.ImageSettings.Text = "Image Settings";
            this.ImageSettings.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button2.Location = new System.Drawing.Point(250, 225);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(102, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button_Save_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Location = new System.Drawing.Point(17, 225);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // Group_Capture
            // 
            this.Group_Capture.Controls.Add(this.label2);
            this.Group_Capture.Controls.Add(this.DropDown_FullScreenMode);
            this.Group_Capture.Controls.Add(this.CheckBox_postProcess);
            this.Group_Capture.Location = new System.Drawing.Point(8, 61);
            this.Group_Capture.Name = "Group_Capture";
            this.Group_Capture.Size = new System.Drawing.Size(350, 92);
            this.Group_Capture.TabIndex = 15;
            this.Group_Capture.TabStop = false;
            this.Group_Capture.Text = "Capturing Options";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Full Screen Capture Mode";
            // 
            // DropDown_FullScreenMode
            // 
            this.DropDown_FullScreenMode.FormattingEnabled = true;
            this.DropDown_FullScreenMode.Items.AddRange(new object[] {
            "All Monitors",
            "Primary Monitor",
            "Monitor with Mouse"});
            this.DropDown_FullScreenMode.Location = new System.Drawing.Point(7, 39);
            this.DropDown_FullScreenMode.Name = "DropDown_FullScreenMode";
            this.DropDown_FullScreenMode.Size = new System.Drawing.Size(337, 21);
            this.DropDown_FullScreenMode.TabIndex = 1;
            this.DropDown_FullScreenMode.SelectedIndexChanged += new System.EventHandler(this.DropDown_FullScreenMode_SelectedIndexChanged);
            // 
            // CheckBox_postProcess
            // 
            this.CheckBox_postProcess.AutoSize = true;
            this.CheckBox_postProcess.Location = new System.Drawing.Point(7, 66);
            this.CheckBox_postProcess.Name = "CheckBox_postProcess";
            this.CheckBox_postProcess.Size = new System.Drawing.Size(169, 17);
            this.CheckBox_postProcess.TabIndex = 0;
            this.CheckBox_postProcess.Text = "Post Process Artifact Removal";
            this.CheckBox_postProcess.UseVisualStyleBackColor = true;
            this.CheckBox_postProcess.CheckedChanged += new System.EventHandler(this.CheckBox_postProcess_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButton5);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 49);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Default Image Format";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(170, 20);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(47, 17);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "TIFF";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(121, 20);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(42, 17);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "GIF";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(66, 20);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(48, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "PNG";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(7, 20);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(52, 17);
            this.radioButton5.TabIndex = 0;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "JPEG";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged);
            // 
            // CaptureDoneSettings
            // 
            this.CaptureDoneSettings.Controls.Add(this.button4);
            this.CaptureDoneSettings.Controls.Add(this.button3);
            this.CaptureDoneSettings.Controls.Add(this.OnImageDoneGroup);
            this.CaptureDoneSettings.Location = new System.Drawing.Point(4, 40);
            this.CaptureDoneSettings.Name = "CaptureDoneSettings";
            this.CaptureDoneSettings.Size = new System.Drawing.Size(367, 252);
            this.CaptureDoneSettings.TabIndex = 2;
            this.CaptureDoneSettings.Text = "Capture Done";
            this.CaptureDoneSettings.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button4.Location = new System.Drawing.Point(250, 225);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(102, 23);
            this.button4.TabIndex = 18;
            this.button4.Text = "Save";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Button_Save_Click);
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button3.Location = new System.Drawing.Point(17, 225);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(96, 23);
            this.button3.TabIndex = 17;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // OnImageDoneGroup
            // 
            this.OnImageDoneGroup.Controls.Add(this.DropDown_NotifiMode);
            this.OnImageDoneGroup.Controls.Add(this.label7);
            this.OnImageDoneGroup.Controls.Add(this.label3);
            this.OnImageDoneGroup.Controls.Add(this.Button_SelectSoundPath);
            this.OnImageDoneGroup.Controls.Add(this.TextBox_SoundPath);
            this.OnImageDoneGroup.Controls.Add(this.CheckBox_CustomConfirmSound);
            this.OnImageDoneGroup.Controls.Add(this.CheckBox_ToClipboard);
            this.OnImageDoneGroup.Controls.Add(this.CheckBox_AutoUpload);
            this.OnImageDoneGroup.Controls.Add(this.CheckBox_SaveToHDD);
            this.OnImageDoneGroup.Controls.Add(this.Button_SelectPath);
            this.OnImageDoneGroup.Controls.Add(this.label1);
            this.OnImageDoneGroup.Controls.Add(this.TextBox_SavePath);
            this.OnImageDoneGroup.Location = new System.Drawing.Point(8, 3);
            this.OnImageDoneGroup.Name = "OnImageDoneGroup";
            this.OnImageDoneGroup.Size = new System.Drawing.Size(356, 216);
            this.OnImageDoneGroup.TabIndex = 14;
            this.OnImageDoneGroup.TabStop = false;
            this.OnImageDoneGroup.Text = "On Image Done";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 171);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Custom Confrimation Sound Path";
            // 
            // Button_SelectSoundPath
            // 
            this.Button_SelectSoundPath.BackColor = System.Drawing.SystemColors.Control;
            this.Button_SelectSoundPath.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Button_SelectSoundPath.Location = new System.Drawing.Point(322, 190);
            this.Button_SelectSoundPath.Name = "Button_SelectSoundPath";
            this.Button_SelectSoundPath.Size = new System.Drawing.Size(28, 20);
            this.Button_SelectSoundPath.TabIndex = 15;
            this.Button_SelectSoundPath.Text = "...";
            this.Button_SelectSoundPath.UseVisualStyleBackColor = false;
            this.Button_SelectSoundPath.Click += new System.EventHandler(this.Button_SelectSoundPath_Click);
            // 
            // TextBox_SoundPath
            // 
            this.TextBox_SoundPath.Location = new System.Drawing.Point(6, 190);
            this.TextBox_SoundPath.Name = "TextBox_SoundPath";
            this.TextBox_SoundPath.Size = new System.Drawing.Size(317, 20);
            this.TextBox_SoundPath.TabIndex = 14;
            this.TextBox_SoundPath.TextChanged += new System.EventHandler(this.TextBox_SoundPath_TextChanged);
            // 
            // CheckBox_CustomConfirmSound
            // 
            this.CheckBox_CustomConfirmSound.AutoSize = true;
            this.CheckBox_CustomConfirmSound.Location = new System.Drawing.Point(171, 170);
            this.CheckBox_CustomConfirmSound.Name = "CheckBox_CustomConfirmSound";
            this.CheckBox_CustomConfirmSound.Size = new System.Drawing.Size(178, 17);
            this.CheckBox_CustomConfirmSound.TabIndex = 13;
            this.CheckBox_CustomConfirmSound.Text = "Use Custom Confrimation Sound";
            this.CheckBox_CustomConfirmSound.UseVisualStyleBackColor = true;
            this.CheckBox_CustomConfirmSound.CheckedChanged += new System.EventHandler(this.CheckBox_CustomConfirmSound_CheckedChanged);
            // 
            // CheckBox_ToClipboard
            // 
            this.CheckBox_ToClipboard.AutoSize = true;
            this.CheckBox_ToClipboard.Location = new System.Drawing.Point(31, 104);
            this.CheckBox_ToClipboard.Name = "CheckBox_ToClipboard";
            this.CheckBox_ToClipboard.Size = new System.Drawing.Size(193, 17);
            this.CheckBox_ToClipboard.TabIndex = 10;
            this.CheckBox_ToClipboard.Text = "Copy Link to Clipboard after Upload";
            this.CheckBox_ToClipboard.UseVisualStyleBackColor = true;
            this.CheckBox_ToClipboard.CheckedChanged += new System.EventHandler(this.CheckBox_ToClipboard_CheckedChanged);
            // 
            // CheckBox_AutoUpload
            // 
            this.CheckBox_AutoUpload.AutoSize = true;
            this.CheckBox_AutoUpload.Location = new System.Drawing.Point(9, 81);
            this.CheckBox_AutoUpload.Name = "CheckBox_AutoUpload";
            this.CheckBox_AutoUpload.Size = new System.Drawing.Size(163, 17);
            this.CheckBox_AutoUpload.TabIndex = 9;
            this.CheckBox_AutoUpload.Text = "Auto-Upload to Image Server";
            this.CheckBox_AutoUpload.UseVisualStyleBackColor = true;
            this.CheckBox_AutoUpload.CheckedChanged += new System.EventHandler(this.CheckBox_AutoUpload_CheckedChanged);
            // 
            // CheckBox_SaveToHDD
            // 
            this.CheckBox_SaveToHDD.AutoSize = true;
            this.CheckBox_SaveToHDD.Location = new System.Drawing.Point(6, 19);
            this.CheckBox_SaveToHDD.Name = "CheckBox_SaveToHDD";
            this.CheckBox_SaveToHDD.Size = new System.Drawing.Size(144, 17);
            this.CheckBox_SaveToHDD.TabIndex = 11;
            this.CheckBox_SaveToHDD.Text = "Save Image to Harddrive";
            this.CheckBox_SaveToHDD.UseVisualStyleBackColor = true;
            this.CheckBox_SaveToHDD.CheckedChanged += new System.EventHandler(this.CheckBox_SaveToHDD_CheckedChanged);
            // 
            // Button_SelectPath
            // 
            this.Button_SelectPath.BackColor = System.Drawing.SystemColors.Control;
            this.Button_SelectPath.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Button_SelectPath.Location = new System.Drawing.Point(322, 55);
            this.Button_SelectPath.Name = "Button_SelectPath";
            this.Button_SelectPath.Size = new System.Drawing.Size(28, 20);
            this.Button_SelectPath.TabIndex = 8;
            this.Button_SelectPath.Text = "...";
            this.Button_SelectPath.UseVisualStyleBackColor = false;
            this.Button_SelectPath.Click += new System.EventHandler(this.Button_SelectPath_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Screenshot Path";
            // 
            // TextBox_SavePath
            // 
            this.TextBox_SavePath.Location = new System.Drawing.Point(6, 55);
            this.TextBox_SavePath.Name = "TextBox_SavePath";
            this.TextBox_SavePath.Size = new System.Drawing.Size(317, 20);
            this.TextBox_SavePath.TabIndex = 6;
            // 
            // StartupSettings
            // 
            this.StartupSettings.Controls.Add(this.button6);
            this.StartupSettings.Controls.Add(this.button5);
            this.StartupSettings.Controls.Add(this.Group_Startup);
            this.StartupSettings.Location = new System.Drawing.Point(4, 40);
            this.StartupSettings.Name = "StartupSettings";
            this.StartupSettings.Size = new System.Drawing.Size(367, 252);
            this.StartupSettings.TabIndex = 3;
            this.StartupSettings.Text = "Startup Settings";
            this.StartupSettings.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button6.Location = new System.Drawing.Point(250, 225);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(102, 23);
            this.button6.TabIndex = 19;
            this.button6.Text = "Save";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Button_Save_Click);
            // 
            // button5
            // 
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button5.Location = new System.Drawing.Point(17, 225);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(96, 23);
            this.button5.TabIndex = 18;
            this.button5.Text = "Cancel";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // Group_Startup
            // 
            this.Group_Startup.Controls.Add(this.CheckBox_StartInTray);
            this.Group_Startup.Controls.Add(this.CheckBox_StartWithWindows);
            this.Group_Startup.Location = new System.Drawing.Point(8, 3);
            this.Group_Startup.Name = "Group_Startup";
            this.Group_Startup.Size = new System.Drawing.Size(350, 72);
            this.Group_Startup.TabIndex = 10;
            this.Group_Startup.TabStop = false;
            this.Group_Startup.Text = "Startup";
            // 
            // CheckBox_StartInTray
            // 
            this.CheckBox_StartInTray.AutoSize = true;
            this.CheckBox_StartInTray.Location = new System.Drawing.Point(9, 42);
            this.CheckBox_StartInTray.Name = "CheckBox_StartInTray";
            this.CheckBox_StartInTray.Size = new System.Drawing.Size(175, 17);
            this.CheckBox_StartInTray.TabIndex = 1;
            this.CheckBox_StartInTray.Text = "Start in System Tray (Minimized)";
            this.CheckBox_StartInTray.UseVisualStyleBackColor = true;
            this.CheckBox_StartInTray.CheckedChanged += new System.EventHandler(this.CheckBox_StartInTray_CheckedChanged);
            // 
            // CheckBox_StartWithWindows
            // 
            this.CheckBox_StartWithWindows.AutoSize = true;
            this.CheckBox_StartWithWindows.Location = new System.Drawing.Point(9, 19);
            this.CheckBox_StartWithWindows.Name = "CheckBox_StartWithWindows";
            this.CheckBox_StartWithWindows.Size = new System.Drawing.Size(117, 17);
            this.CheckBox_StartWithWindows.TabIndex = 0;
            this.CheckBox_StartWithWindows.Text = "Start with Windows";
            this.CheckBox_StartWithWindows.UseVisualStyleBackColor = true;
            this.CheckBox_StartWithWindows.CheckedChanged += new System.EventHandler(this.CheckBox_StartWithWindows_CheckedChanged);
            // 
            // WebMSettings
            // 
            this.WebMSettings.Controls.Add(this.groupBox2);
            this.WebMSettings.Controls.Add(this.button8);
            this.WebMSettings.Controls.Add(this.button7);
            this.WebMSettings.Location = new System.Drawing.Point(4, 40);
            this.WebMSettings.Name = "WebMSettings";
            this.WebMSettings.Size = new System.Drawing.Size(367, 252);
            this.WebMSettings.TabIndex = 4;
            this.WebMSettings.Text = "WebM Settings";
            this.WebMSettings.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CheckBox_WebM_RecCursor);
            this.groupBox2.Controls.Add(this.Label_WebM_CurrentFPS);
            this.groupBox2.Controls.Add(this.TrackBar_Webm_Framerate);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.TrackBar_WebmQuality);
            this.groupBox2.Location = new System.Drawing.Point(4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(360, 215);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "WebM Settings";
            // 
            // CheckBox_WebM_RecCursor
            // 
            this.CheckBox_WebM_RecCursor.AutoSize = true;
            this.CheckBox_WebM_RecCursor.Location = new System.Drawing.Point(13, 168);
            this.CheckBox_WebM_RecCursor.Name = "CheckBox_WebM_RecCursor";
            this.CheckBox_WebM_RecCursor.Size = new System.Drawing.Size(94, 17);
            this.CheckBox_WebM_RecCursor.TabIndex = 5;
            this.CheckBox_WebM_RecCursor.Text = "Record Cursor";
            this.CheckBox_WebM_RecCursor.UseVisualStyleBackColor = true;
            this.CheckBox_WebM_RecCursor.CheckedChanged += new System.EventHandler(this.CheckBox_WebM_RecCursor_CheckedChanged);
            // 
            // Label_WebM_CurrentFPS
            // 
            this.Label_WebM_CurrentFPS.AutoSize = true;
            this.Label_WebM_CurrentFPS.Location = new System.Drawing.Point(329, 149);
            this.Label_WebM_CurrentFPS.Name = "Label_WebM_CurrentFPS";
            this.Label_WebM_CurrentFPS.Size = new System.Drawing.Size(19, 13);
            this.Label_WebM_CurrentFPS.TabIndex = 4;
            this.Label_WebM_CurrentFPS.Text = "25";
            // 
            // TrackBar_Webm_Framerate
            // 
            this.TrackBar_Webm_Framerate.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.TrackBar_Webm_Framerate.Location = new System.Drawing.Point(6, 117);
            this.TrackBar_Webm_Framerate.Maximum = 60;
            this.TrackBar_Webm_Framerate.Minimum = 10;
            this.TrackBar_Webm_Framerate.Name = "TrackBar_Webm_Framerate";
            this.TrackBar_Webm_Framerate.Size = new System.Drawing.Size(348, 45);
            this.TrackBar_Webm_Framerate.SmallChange = 5;
            this.TrackBar_Webm_Framerate.TabIndex = 3;
            this.TrackBar_Webm_Framerate.Value = 25;
            this.TrackBar_Webm_Framerate.Scroll += new System.EventHandler(this.TrackBar_Webm_Framerate_Scroll);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(205, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Recording Framerate (Frames per second)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Quality";
            // 
            // TrackBar_WebmQuality
            // 
            this.TrackBar_WebmQuality.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.TrackBar_WebmQuality.Location = new System.Drawing.Point(6, 42);
            this.TrackBar_WebmQuality.Minimum = 1;
            this.TrackBar_WebmQuality.Name = "TrackBar_WebmQuality";
            this.TrackBar_WebmQuality.Size = new System.Drawing.Size(349, 45);
            this.TrackBar_WebmQuality.TabIndex = 0;
            this.TrackBar_WebmQuality.Value = 7;
            this.TrackBar_WebmQuality.Scroll += new System.EventHandler(this.TrackBar_WebmQuality_Scroll);
            // 
            // button8
            // 
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button8.Location = new System.Drawing.Point(250, 225);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(102, 23);
            this.button8.TabIndex = 18;
            this.button8.Text = "Save";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Button_Save_Click);
            // 
            // button7
            // 
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button7.Location = new System.Drawing.Point(17, 225);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(96, 23);
            this.button7.TabIndex = 17;
            this.button7.Text = "Cancel";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // HotkeySettings
            // 
            this.HotkeySettings.Controls.Add(this.groupBox3);
            this.HotkeySettings.Controls.Add(this.button9);
            this.HotkeySettings.Controls.Add(this.button10);
            this.HotkeySettings.Location = new System.Drawing.Point(4, 40);
            this.HotkeySettings.Name = "HotkeySettings";
            this.HotkeySettings.Size = new System.Drawing.Size(367, 252);
            this.HotkeySettings.TabIndex = 5;
            this.HotkeySettings.Text = "Hotkey Settigns";
            this.HotkeySettings.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Button_HKey_UploadClip);
            this.groupBox3.Controls.Add(this.Label_HKey_UploadClip);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.Button_HKey_WebMCaptureStop);
            this.groupBox3.Controls.Add(this.Label_HKey_WebMCaptureStop);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.Button_HKey_WebMCapture);
            this.groupBox3.Controls.Add(this.Label_HKey_WebMCapture);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.Button_HKey_AreaCapture);
            this.groupBox3.Controls.Add(this.Label_HKey_AreaCapture);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.Button_HKey_ScreenCap);
            this.groupBox3.Controls.Add(this.Label_HKey_ScreenCap);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(355, 216);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Hotkey Settings";
            // 
            // Button_HKey_UploadClip
            // 
            this.Button_HKey_UploadClip.FlatAppearance.BorderSize = 0;
            this.Button_HKey_UploadClip.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Button_HKey_UploadClip.Location = new System.Drawing.Point(296, 147);
            this.Button_HKey_UploadClip.Margin = new System.Windows.Forms.Padding(0);
            this.Button_HKey_UploadClip.Name = "Button_HKey_UploadClip";
            this.Button_HKey_UploadClip.Size = new System.Drawing.Size(53, 21);
            this.Button_HKey_UploadClip.TabIndex = 14;
            this.Button_HKey_UploadClip.Text = "Change";
            this.Button_HKey_UploadClip.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.Button_HKey_UploadClip.UseVisualStyleBackColor = true;
            this.Button_HKey_UploadClip.Click += new System.EventHandler(this.Button_HKey_UploadClip_Click);
            // 
            // Label_HKey_UploadClip
            // 
            this.Label_HKey_UploadClip.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Label_HKey_UploadClip.Location = new System.Drawing.Point(104, 147);
            this.Label_HKey_UploadClip.Name = "Label_HKey_UploadClip";
            this.Label_HKey_UploadClip.Size = new System.Drawing.Size(189, 21);
            this.Label_HKey_UploadClip.TabIndex = 13;
            this.Label_HKey_UploadClip.Text = "CTRL + ALT + SHIFT + K";
            this.Label_HKey_UploadClip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 151);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 13);
            this.label15.TabIndex = 12;
            this.label15.Text = "Upload Clipboard:";
            // 
            // Button_HKey_WebMCaptureStop
            // 
            this.Button_HKey_WebMCaptureStop.FlatAppearance.BorderSize = 0;
            this.Button_HKey_WebMCaptureStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Button_HKey_WebMCaptureStop.Location = new System.Drawing.Point(296, 117);
            this.Button_HKey_WebMCaptureStop.Margin = new System.Windows.Forms.Padding(0);
            this.Button_HKey_WebMCaptureStop.Name = "Button_HKey_WebMCaptureStop";
            this.Button_HKey_WebMCaptureStop.Size = new System.Drawing.Size(53, 21);
            this.Button_HKey_WebMCaptureStop.TabIndex = 11;
            this.Button_HKey_WebMCaptureStop.Text = "Change";
            this.Button_HKey_WebMCaptureStop.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.Button_HKey_WebMCaptureStop.UseVisualStyleBackColor = true;
            this.Button_HKey_WebMCaptureStop.Click += new System.EventHandler(this.Button_HKey_WebMCaptureStop_Click);
            // 
            // Label_HKey_WebMCaptureStop
            // 
            this.Label_HKey_WebMCaptureStop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Label_HKey_WebMCaptureStop.Location = new System.Drawing.Point(104, 117);
            this.Label_HKey_WebMCaptureStop.Name = "Label_HKey_WebMCaptureStop";
            this.Label_HKey_WebMCaptureStop.Size = new System.Drawing.Size(189, 21);
            this.Label_HKey_WebMCaptureStop.TabIndex = 10;
            this.Label_HKey_WebMCaptureStop.Text = "CTRL + ALT + SHIFT + K";
            this.Label_HKey_WebMCaptureStop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 121);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Stop Recording: ";
            // 
            // Button_HKey_WebMCapture
            // 
            this.Button_HKey_WebMCapture.FlatAppearance.BorderSize = 0;
            this.Button_HKey_WebMCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Button_HKey_WebMCapture.Location = new System.Drawing.Point(296, 87);
            this.Button_HKey_WebMCapture.Margin = new System.Windows.Forms.Padding(0);
            this.Button_HKey_WebMCapture.Name = "Button_HKey_WebMCapture";
            this.Button_HKey_WebMCapture.Size = new System.Drawing.Size(53, 21);
            this.Button_HKey_WebMCapture.TabIndex = 8;
            this.Button_HKey_WebMCapture.Text = "Change";
            this.Button_HKey_WebMCapture.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.Button_HKey_WebMCapture.UseVisualStyleBackColor = true;
            this.Button_HKey_WebMCapture.Click += new System.EventHandler(this.Button_HKey_WebMCapture_Click);
            // 
            // Label_HKey_WebMCapture
            // 
            this.Label_HKey_WebMCapture.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Label_HKey_WebMCapture.Location = new System.Drawing.Point(104, 87);
            this.Label_HKey_WebMCapture.Name = "Label_HKey_WebMCapture";
            this.Label_HKey_WebMCapture.Size = new System.Drawing.Size(189, 21);
            this.Label_HKey_WebMCapture.TabIndex = 7;
            this.Label_HKey_WebMCapture.Text = "CTRL + ALT + SHIFT + K";
            this.Label_HKey_WebMCapture.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Record WebM: ";
            // 
            // Button_HKey_AreaCapture
            // 
            this.Button_HKey_AreaCapture.FlatAppearance.BorderSize = 0;
            this.Button_HKey_AreaCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Button_HKey_AreaCapture.Location = new System.Drawing.Point(296, 57);
            this.Button_HKey_AreaCapture.Margin = new System.Windows.Forms.Padding(0);
            this.Button_HKey_AreaCapture.Name = "Button_HKey_AreaCapture";
            this.Button_HKey_AreaCapture.Size = new System.Drawing.Size(53, 21);
            this.Button_HKey_AreaCapture.TabIndex = 5;
            this.Button_HKey_AreaCapture.Text = "Change";
            this.Button_HKey_AreaCapture.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.Button_HKey_AreaCapture.UseVisualStyleBackColor = true;
            this.Button_HKey_AreaCapture.Click += new System.EventHandler(this.Button_HKey_AreaCapture_Click);
            // 
            // Label_HKey_AreaCapture
            // 
            this.Label_HKey_AreaCapture.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Label_HKey_AreaCapture.Location = new System.Drawing.Point(104, 57);
            this.Label_HKey_AreaCapture.Name = "Label_HKey_AreaCapture";
            this.Label_HKey_AreaCapture.Size = new System.Drawing.Size(189, 21);
            this.Label_HKey_AreaCapture.TabIndex = 4;
            this.Label_HKey_AreaCapture.Text = "CTRL + ALT + SHIFT + K";
            this.Label_HKey_AreaCapture.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Area Capture: ";
            // 
            // Button_HKey_ScreenCap
            // 
            this.Button_HKey_ScreenCap.FlatAppearance.BorderSize = 0;
            this.Button_HKey_ScreenCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Button_HKey_ScreenCap.Location = new System.Drawing.Point(296, 27);
            this.Button_HKey_ScreenCap.Margin = new System.Windows.Forms.Padding(0);
            this.Button_HKey_ScreenCap.Name = "Button_HKey_ScreenCap";
            this.Button_HKey_ScreenCap.Size = new System.Drawing.Size(53, 21);
            this.Button_HKey_ScreenCap.TabIndex = 2;
            this.Button_HKey_ScreenCap.Text = "Change";
            this.Button_HKey_ScreenCap.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.Button_HKey_ScreenCap.UseVisualStyleBackColor = true;
            this.Button_HKey_ScreenCap.Click += new System.EventHandler(this.Button_HKey_ScreenCap_Click);
            // 
            // Label_HKey_ScreenCap
            // 
            this.Label_HKey_ScreenCap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Label_HKey_ScreenCap.Location = new System.Drawing.Point(104, 27);
            this.Label_HKey_ScreenCap.Name = "Label_HKey_ScreenCap";
            this.Label_HKey_ScreenCap.Size = new System.Drawing.Size(189, 21);
            this.Label_HKey_ScreenCap.TabIndex = 1;
            this.Label_HKey_ScreenCap.Text = "CTRL + ALT + SHIFT + K";
            this.Label_HKey_ScreenCap.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Screen Capture: ";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // button9
            // 
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button9.Location = new System.Drawing.Point(250, 225);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(102, 23);
            this.button9.TabIndex = 20;
            this.button9.Text = "Save";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.Button_Save_Click);
            // 
            // button10
            // 
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button10.Location = new System.Drawing.Point(17, 225);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(96, 23);
            this.button10.TabIndex = 19;
            this.button10.Text = "Cancel";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Notification Mode";
            // 
            // DropDown_NotifiMode
            // 
            this.DropDown_NotifiMode.FormattingEnabled = true;
            this.DropDown_NotifiMode.Items.AddRange(new object[] {
            "Windows Notifications",
            "Progress Bars"});
            this.DropDown_NotifiMode.Location = new System.Drawing.Point(103, 136);
            this.DropDown_NotifiMode.Name = "DropDown_NotifiMode";
            this.DropDown_NotifiMode.Size = new System.Drawing.Size(220, 21);
            this.DropDown_NotifiMode.TabIndex = 18;
            this.DropDown_NotifiMode.SelectedIndexChanged += new System.EventHandler(this.DropDown_NotifiMode_SelectedIndexChanged);
            // 
            // MasterSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(374, 297);
            this.Controls.Add(this.TabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MasterSettings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.MasterSettings_Load);
            this.TabControl.ResumeLayout(false);
            this.ConnectionSettings.ResumeLayout(false);
            this.Group_Connection.ResumeLayout(false);
            this.Group_Connection.PerformLayout();
            this.ImageSettings.ResumeLayout(false);
            this.Group_Capture.ResumeLayout(false);
            this.Group_Capture.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.CaptureDoneSettings.ResumeLayout(false);
            this.OnImageDoneGroup.ResumeLayout(false);
            this.OnImageDoneGroup.PerformLayout();
            this.StartupSettings.ResumeLayout(false);
            this.Group_Startup.ResumeLayout(false);
            this.Group_Startup.PerformLayout();
            this.WebMSettings.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_Webm_Framerate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBar_WebmQuality)).EndInit();
            this.HotkeySettings.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage ConnectionSettings;
        private System.Windows.Forms.TabPage ImageSettings;
        private System.Windows.Forms.TabPage CaptureDoneSettings;
        private System.Windows.Forms.TabPage StartupSettings;
        private System.Windows.Forms.GroupBox Group_Connection;
        private System.Windows.Forms.TextBox TextBox_APIKey;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TextBox_Password;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TextBox_Email;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TextBox_URI;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.GroupBox Group_Capture;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox DropDown_FullScreenMode;
        private System.Windows.Forms.CheckBox CheckBox_postProcess;
        private System.Windows.Forms.Button Button_Cancel;
        private System.Windows.Forms.Button Button_Save;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox OnImageDoneGroup;
        private System.Windows.Forms.CheckBox CheckBox_ToClipboard;
        private System.Windows.Forms.CheckBox CheckBox_AutoUpload;
        private System.Windows.Forms.CheckBox CheckBox_SaveToHDD;
        private System.Windows.Forms.Button Button_SelectPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBox_SavePath;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox Group_Startup;
        private System.Windows.Forms.CheckBox CheckBox_StartWithWindows;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.CheckBox CheckBox_StartInTray;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Button_SelectSoundPath;
        private System.Windows.Forms.TextBox TextBox_SoundPath;
        private System.Windows.Forms.CheckBox CheckBox_CustomConfirmSound;
        private System.Windows.Forms.TabPage WebMSettings;
        private System.Windows.Forms.TabPage HotkeySettings;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TrackBar TrackBar_WebmQuality;
        private System.Windows.Forms.Label Label_WebM_CurrentFPS;
        private System.Windows.Forms.TrackBar TrackBar_Webm_Framerate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox CheckBox_WebM_RecCursor;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button Button_HKey_ScreenCap;
        private System.Windows.Forms.Label Label_HKey_ScreenCap;
        private System.Windows.Forms.Button Button_HKey_WebMCaptureStop;
        private System.Windows.Forms.Label Label_HKey_WebMCaptureStop;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button Button_HKey_WebMCapture;
        private System.Windows.Forms.Label Label_HKey_WebMCapture;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button Button_HKey_AreaCapture;
        private System.Windows.Forms.Label Label_HKey_AreaCapture;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button Button_HKey_UploadClip;
        private System.Windows.Forms.Label Label_HKey_UploadClip;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox DropDown_NotifiMode;
        private System.Windows.Forms.Label label7;
    }
}