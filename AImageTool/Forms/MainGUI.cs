﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Drawing.Imaging;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Threading;
using System.Net;
using System.IO;

namespace AImageTool
{
    public partial class MainGUI : Form
    {
        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr window, int index, int value);
        [DllImport("user32.dll")]
        public static extern int GetWindowLong(IntPtr window, int index);
        const int GWL_EXSTYLE = -20;
        const int WS_EX_TOOLWINDOW = 0x00000080;
        const int WS_EX_APPWINDOW = 0x00040000;


        private NotifyIcon notifyIcon;
        private ProgramCore Core;

        bool checkingUpdate = false;

   
        delegate void CloseSplashScreenCallback(bool flag);
        delegate void ChangeIconCallback(Icon ic);

        public MainGUI()
        {

            InitializeComponent();

            Process[] pname = Process.GetProcessesByName("ShareDat.exe");
            if (pname.Length > 0)
            {
                DialogResult dgres = MessageBox.Show("Another instance of ShareDat appears to be running. Please close all other instances.", "Cheesy overeagerness!", MessageBoxButtons.OK);
                if (dgres == DialogResult.OK)
                {
                    Application.Exit();
                }
            }

            this.Owner = new Form();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.components = new System.ComponentModel.Container();
            notifyIcon = new NotifyIcon(this.components);

            notifyIcon.Icon = Properties.Resources.sharedat;
            notifyIcon.Text = "AImageTool pre-alpha";
            

            notifyIcon.Click += notifyIcon_Click;
            notifyIcon.BalloonTipClicked += notifyIcon_BalloonTipClicked;
            // init programm
            Core = new ProgramCore(this);  
            
            // this.Hide();
            this.Hide();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //this.FormBorderStyle = FormBorderStyle.SizableToolWindow;
            //this.AllowTransparency = true;
            this.BackColor = Color.Transparent;
            this.FormBorderStyle = FormBorderStyle.None;
            this.Owner = new Form();

            Core.DoDelayed(() =>
            {
             //   CheckNewestVersion(this);

            }, 300);

            Core.DoDelayed(() => 
            {
                HideSplashScreen(!ProgramCore.CurUserSettings.StartInTray);

            }, 3000);
           



        }


        private static async Task CheckNewestVersion(MainGUI parent)
        {

                // Check for the newest version of the client. Replace link with a link to your version number txt.
                parent.checkingUpdate = true;
                WebClient client = new WebClient();
                Stream stream = await client.OpenReadTaskAsync(new Uri("https://allions.net/angularimg/downloads/versionData.txt",UriKind.Absolute));
                StreamReader reader = new StreamReader(stream);
                String content = reader.ReadToEnd();

                string[] versionParams = content.Split(':');

                // dirty, only windows version here anyway
                if (Int32.Parse(versionParams[1]) > Program.VersionNumber)
                {
                    DialogResult dgres = MessageBox.Show("There is a new version of ShareDat available. Do you want to update now?", "Cheesy new version! Yum yum!", MessageBoxButtons.YesNo);
                    if (dgres == DialogResult.Yes)
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.Arguments = Program.VersionNumber.ToString();
                        startInfo.FileName= "AutoUpdater.exe";
                        Process.Start(startInfo);
                        Application.Exit();
                    }
                }
                parent.checkingUpdate = false;
            

        }

        /// <summary>
        /// Amazing delegate magic to allow cross thread form hiding! ;)
        /// https://msdn.microsoft.com/en-us/library/ms171728%28v=vs.80%29.aspx
        /// </summary>
        public void HideSplashScreen(bool ShowSettings)
        {
            // wait
            while (checkingUpdate) { Thread.Sleep(100); }

            try {
                if (this.InvokeRequired)
                {
                    CloseSplashScreenCallback cb = new CloseSplashScreenCallback(HideSplashScreen);
                    this.Invoke(cb, new object[] { ShowSettings });
                }
                else
                {
                    this.TransparencyKey = Color.Transparent;
                    this.WindowState = FormWindowState.Minimized;
                    this.Visible = false;
                    notifyIcon.Visible = true;
                    ShowInTaskbar = false;
                    int windowStyle = GetWindowLong(Handle, GWL_EXSTYLE);
                    SetWindowLong(Handle, GWL_EXSTYLE, windowStyle | WS_EX_TOOLWINDOW);
                    if (ShowSettings)
                    {
                        Forms.MasterSettings ms = new Forms.MasterSettings();
                        ms.Show();
                    }
                }
            }
            // most likely the programm got terminated at this point
            catch { }
            
        }

        public void ChangeIcon(Icon ic)
        {
            if(this.InvokeRequired)
            {
                ChangeIconCallback cb = new ChangeIconCallback(ChangeIcon);
                this.Invoke(cb, new object[] { ic });
            }
            else
            {
                this.Icon = ic;
                notifyIcon.Icon = ic;
            }
        }


        Forms.NotifyIconSelector selector;
        /// <summary>
        /// Defines what happens when we click on the notify icon
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void notifyIcon_Click(object sender, EventArgs e)
        {
            selector = new Forms.NotifyIconSelector(this);

        }

        internal void NotifyIconSelectorLostFocus()
        {
            if (selector != null)
            {
                selector.Hide();
                selector.Dispose();
                selector = null;
            }
        }

        /// <summary>
        /// Defines what happens when we click the baloon tooltip
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void notifyIcon_BalloonTipClicked(object sender, EventArgs e)
        {
            if (Core.lastUrl != "")
            {
                Process.Start(Core.lastUrl);
            }

        }

        /// <summary>
        /// Shows a notification from the tray
        /// </summary>
        /// <param name="message"></param>
        public void ShowNotificationFromTray(string message, bool sound = false, bool positiveSound = false)
        {
            notifyIcon.BalloonTipText = message;
            notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon.BalloonTipTitle = AsyncImageOperation.Status;
            notifyIcon.ShowBalloonTip(100);
            if (sound)
            {
                if (positiveSound)
                {
                    if (ProgramCore.CurUserSettings.UseCustomShareSound)
                    {
                        ProgramCore.CustomDoneSound.Play();
                    }
                    else
                    {
                        System.Media.SystemSounds.Asterisk.Play();
                    }
                }
                else
                    System.Media.SystemSounds.Hand.Play();
            }
        }

        /// <summary>
        /// Plays the positive notification sound
        /// </summary>
        public void PlayPositiveSound()
        {
            if (ProgramCore.CurUserSettings.UseCustomShareSound)
            {
                ProgramCore.CustomDoneSound.Play();
            }
            else
            {
                System.Media.SystemSounds.Asterisk.Play();
            }
        }
    
        private void MainGUI_Load(object sender, EventArgs e)
        {

        }
    }
}

