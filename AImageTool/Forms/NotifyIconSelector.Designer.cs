﻿namespace AImageTool.Forms
{
    partial class NotifyIconSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_ConnectionSettings = new System.Windows.Forms.Button();
            this.Button_ImageSettings = new System.Windows.Forms.Button();
            this.Button_OnDoneSettings = new System.Windows.Forms.Button();
            this.Button_StartupSettings = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Button_WebMSettings = new System.Windows.Forms.Button();
            this.Button_HotkeySettings = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Button_ConnectionSettings
            // 
            this.Button_ConnectionSettings.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Button_ConnectionSettings.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Button_ConnectionSettings.FlatAppearance.BorderSize = 0;
            this.Button_ConnectionSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.Button_ConnectionSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_ConnectionSettings.Location = new System.Drawing.Point(0, 0);
            this.Button_ConnectionSettings.Name = "Button_ConnectionSettings";
            this.Button_ConnectionSettings.Size = new System.Drawing.Size(183, 23);
            this.Button_ConnectionSettings.TabIndex = 0;
            this.Button_ConnectionSettings.Text = "Connection Settings";
            this.Button_ConnectionSettings.UseVisualStyleBackColor = false;
            this.Button_ConnectionSettings.Click += new System.EventHandler(this.Button_ConnectionSettings_Click);
            // 
            // Button_ImageSettings
            // 
            this.Button_ImageSettings.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Button_ImageSettings.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Button_ImageSettings.FlatAppearance.BorderSize = 0;
            this.Button_ImageSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.Button_ImageSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_ImageSettings.Location = new System.Drawing.Point(0, 29);
            this.Button_ImageSettings.Name = "Button_ImageSettings";
            this.Button_ImageSettings.Size = new System.Drawing.Size(183, 23);
            this.Button_ImageSettings.TabIndex = 1;
            this.Button_ImageSettings.Text = "Image Settings";
            this.Button_ImageSettings.UseVisualStyleBackColor = false;
            this.Button_ImageSettings.Click += new System.EventHandler(this.Button_ImageSettings_Click);
            // 
            // Button_OnDoneSettings
            // 
            this.Button_OnDoneSettings.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Button_OnDoneSettings.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Button_OnDoneSettings.FlatAppearance.BorderSize = 0;
            this.Button_OnDoneSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.Button_OnDoneSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_OnDoneSettings.Location = new System.Drawing.Point(0, 58);
            this.Button_OnDoneSettings.Name = "Button_OnDoneSettings";
            this.Button_OnDoneSettings.Size = new System.Drawing.Size(183, 23);
            this.Button_OnDoneSettings.TabIndex = 2;
            this.Button_OnDoneSettings.Text = "OnDone Settings";
            this.Button_OnDoneSettings.UseVisualStyleBackColor = false;
            this.Button_OnDoneSettings.Click += new System.EventHandler(this.Button_OnDoneSettings_Click);
            // 
            // Button_StartupSettings
            // 
            this.Button_StartupSettings.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Button_StartupSettings.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Button_StartupSettings.FlatAppearance.BorderSize = 0;
            this.Button_StartupSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.Button_StartupSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_StartupSettings.Location = new System.Drawing.Point(0, 87);
            this.Button_StartupSettings.Name = "Button_StartupSettings";
            this.Button_StartupSettings.Size = new System.Drawing.Size(183, 23);
            this.Button_StartupSettings.TabIndex = 3;
            this.Button_StartupSettings.Text = "Startup Settings";
            this.Button_StartupSettings.UseVisualStyleBackColor = false;
            this.Button_StartupSettings.Click += new System.EventHandler(this.Button_StartupSettings_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Crimson;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(0, 174);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(183, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Close ShareDat";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Button_WebMSettings
            // 
            this.Button_WebMSettings.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Button_WebMSettings.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Button_WebMSettings.FlatAppearance.BorderSize = 0;
            this.Button_WebMSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.Button_WebMSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_WebMSettings.Location = new System.Drawing.Point(0, 116);
            this.Button_WebMSettings.Name = "Button_WebMSettings";
            this.Button_WebMSettings.Size = new System.Drawing.Size(183, 23);
            this.Button_WebMSettings.TabIndex = 5;
            this.Button_WebMSettings.Text = "WebM Settings";
            this.Button_WebMSettings.UseVisualStyleBackColor = false;
            this.Button_WebMSettings.Click += new System.EventHandler(this.Button_WebMSettings_Click);
            // 
            // Button_HotkeySettings
            // 
            this.Button_HotkeySettings.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Button_HotkeySettings.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Button_HotkeySettings.FlatAppearance.BorderSize = 0;
            this.Button_HotkeySettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGreen;
            this.Button_HotkeySettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_HotkeySettings.Location = new System.Drawing.Point(0, 145);
            this.Button_HotkeySettings.Name = "Button_HotkeySettings";
            this.Button_HotkeySettings.Size = new System.Drawing.Size(183, 23);
            this.Button_HotkeySettings.TabIndex = 6;
            this.Button_HotkeySettings.Text = "Hotkey Settings";
            this.Button_HotkeySettings.UseVisualStyleBackColor = false;
            this.Button_HotkeySettings.Click += new System.EventHandler(this.Button_HotkeySettings_Click);
            // 
            // NotifyIconSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(183, 197);
            this.Controls.Add(this.Button_HotkeySettings);
            this.Controls.Add(this.Button_WebMSettings);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Button_StartupSettings);
            this.Controls.Add(this.Button_OnDoneSettings);
            this.Controls.Add(this.Button_ImageSettings);
            this.Controls.Add(this.Button_ConnectionSettings);
            this.Name = "NotifyIconSelector";
            this.Text = "NotifyIconSelector";
            this.Load += new System.EventHandler(this.NotifyIconSelector_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Button_ConnectionSettings;
        private System.Windows.Forms.Button Button_ImageSettings;
        private System.Windows.Forms.Button Button_OnDoneSettings;
        private System.Windows.Forms.Button Button_StartupSettings;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Button_WebMSettings;
        private System.Windows.Forms.Button Button_HotkeySettings;
    }
}