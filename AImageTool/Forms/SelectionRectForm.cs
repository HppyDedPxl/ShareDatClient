﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AImageTool.Forms
{
    public partial class SelectionRectForm : Form
    {

        // For showing topmost without stealing focus
        //http://stackoverflow.com/questions/156046/show-a-form-without-stealing-focus/156159#156159
        private const int SW_SHOWNOACTIVATE = 4;
        private const int HWND_TOPMOST = -1;
        private const uint SWP_NOACTIVATE = 0x0010;

        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        static extern bool SetWindowPos(
        int hWnd,             // Window handle
        int hWndInsertAfter,  // Placement-order handle
        int X,                // Horizontal position
        int Y,                // Vertical position
        int cx,               // Width
        int cy,               // Height
        uint uFlags);         // Window positioning flags

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        static void ShowInactiveTopmost(Form frm)
        {
            ShowWindow(frm.Handle, SW_SHOWNOACTIVATE);
            SetWindowPos(frm.Handle.ToInt32(), HWND_TOPMOST,
            frm.Left, frm.Top, frm.Width, frm.Height,
            SWP_NOACTIVATE);
        }

        public SelectionRectForm()
        {
            InitializeComponent();
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.BackColor = Color.White;

            Opacity = 0.3f;
            this.KeyUp += new KeyEventHandler(main_KeyUp);
            ShowInTaskbar = false;

           ShowInactiveTopmost(this);
        }

        private void main_KeyUp(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                Parent.AbortSelection();
            }
        }

        bool recMode;
        public void SetRecordingStyle()
        {
            recMode = true;
            this.BackColor = Color.LimeGreen;
            this.TransparencyKey = Color.LimeGreen;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (recMode)
            {
                Rectangle nRect = this.DisplayRectangle;
                nRect.Width -= 1;
                nRect.Height -= 1;
                e.Graphics.DrawRectangle(Pens.Red, nRect);

            }

        }

        private void SelectionRectForm_Load(object sender, EventArgs e)
        {

        }


        delegate void SetForegroundCallback();
        /// <summary>
        /// Amazing delegate magic to allow cross thread form hiding! ;)
        /// https://msdn.microsoft.com/en-us/library/ms171728%28v=vs.80%29.aspx
        /// </summary>
        public void SetForeground()
        {

            try
            {
                if (this.InvokeRequired)
                {
                    SetForegroundCallback cb = new SetForegroundCallback(SetForeground);
                    this.Invoke(cb, new object[] { });
                }
                else
                {
                    this.BringToFront(); this.TopMost = true;
                    this.Activate();
                }
            }
            // most likely the programm got terminated at this point
            catch { }

        }
    }
}
