﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace AImageTool.Forms
{
    public partial class MasterSettings : Form
    {
        public enum ShareDatAction
        {
            FullScreenshot,
            AreaScreenshot,
            ClipboardUpload,
            AreaWebmRecord,
            AreaWebmRecordStop
        }

        bool startup;

        public MasterSettings()
        {
            InitializeComponent();

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            startup = true;
            StartPosition = FormStartPosition.CenterScreen;

            #region connection data
            // connection data block
            TextBox_URI.Text = ProgramCore.CurUserSettings.Uri;
            TextBox_Email.Text = ProgramCore.CurUserSettings.Email;
            TextBox_Password.Text = ProgramCore.CurUserSettings.Password;
            TextBox_APIKey.Text = ProgramCore.CurUserSettings.Apikey;

            #endregion

            #region image capturing options
            ImageFormat imgFormat = ProgramCore.CurUserSettings.ImgFormat;
            if (imgFormat == ImageFormat.Jpeg)
            {
                radioButton5.Checked = true;
            }
            else if (imgFormat == ImageFormat.Png)
            {
                radioButton1.Checked = true;
            }
            else if (imgFormat == ImageFormat.Gif)
            {
                radioButton2.Checked = true;
            }
            else if (imgFormat == ImageFormat.Tiff)
            {
                radioButton3.Checked = true;
            }

            // Capturing Options
            DropDown_FullScreenMode.DataSource = Enum.GetValues(typeof(FullScreenCaptureMode));
            DropDown_FullScreenMode.DropDownStyle = ComboBoxStyle.DropDownList;
            DropDown_FullScreenMode.SelectedItem = ProgramCore.CurUserSettings.FullCaptureMode;

            CheckBox_postProcess.Checked = ProgramCore.CurUserSettings.PostProcessArtifactRemoval;

            #endregion

            #region on capturing done
            CheckBox_SaveToHDD.Checked = ProgramCore.CurUserSettings.SaveToHDD;
            TextBox_SavePath.Text = ProgramCore.CurUserSettings.DefaultSavePath;

            DropDown_NotifiMode.DataSource = Enum.GetValues(typeof(NotificationMode));
            DropDown_NotifiMode.DropDownStyle = ComboBoxStyle.DropDownList;
            DropDown_NotifiMode.SelectedItem = ProgramCore.CurUserSettings.NotificationMode;

            if (!CheckBox_SaveToHDD.Checked)
            {
                TextBox_SavePath.Enabled = false;
                Button_SelectPath.Enabled = false;
            }
            CheckBox_AutoUpload.Checked = ProgramCore.CurUserSettings.UploadToServer;

            if (!CheckBox_AutoUpload.Checked)
                CheckBox_ToClipboard.Enabled = false;

            CheckBox_ToClipboard.Checked = ProgramCore.CurUserSettings.CopyToClipboard;

            //CheckBox_JohnCena.Checked = ProgramCore.CurUserSettings.JohnCena;
            CheckBox_CustomConfirmSound.Checked = ProgramCore.CurUserSettings.UseCustomShareSound;
            TextBox_SoundPath.Text = ProgramCore.CurUserSettings.CustomShareSoundPath;
            #endregion

            #region on startup options

            CheckBox_StartWithWindows.Checked = ProgramCore.CurUserSettings.StartOnStartup;
            CheckBox_StartInTray.Checked = ProgramCore.CurUserSettings.StartInTray;

            #endregion

            #region Webm Options

            TrackBar_Webm_Framerate.Value = (ProgramCore.CurUserSettings.WebMFramerate<TrackBar_Webm_Framerate.Minimum)? TrackBar_Webm_Framerate.Minimum : ProgramCore.CurUserSettings.WebMFramerate;
            Label_WebM_CurrentFPS.Text = TrackBar_Webm_Framerate.Value.ToString();
            TrackBar_WebmQuality.Value = ProgramCore.CurUserSettings.WebMQuality < TrackBar_WebmQuality.Minimum ? TrackBar_WebmQuality.Minimum : ProgramCore.CurUserSettings.WebMQuality;
            CheckBox_WebM_RecCursor.Checked = ProgramCore.CurUserSettings.WebMrecCursor;

            #endregion

            #region HotkeyOptions

            Label_HKey_ScreenCap.Text = ProgramCore.CurUserSettings.HotkeySettings_FullScreen.Name;

            Label_HKey_AreaCapture.Text = ProgramCore.CurUserSettings.HotkeySettings_AreaScreen.Name;

            Label_HKey_WebMCapture.Text = ProgramCore.CurUserSettings.HotkeySettings_WebmRecord.Name;

            Label_HKey_WebMCaptureStop.Text = ProgramCore.CurUserSettings.HotkeySettings_WebmRecordStop.Name;

            Label_HKey_UploadClip.Text = ProgramCore.CurUserSettings.HotkeySettings_UploadClipBoard.Name;

            #endregion


            startup = false;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
            
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Dispose();
            UserSettings.Load();
        }

        private void Button_Save_Click(object sender, EventArgs e)
        {
            ProgramCore.CurUserSettings.Uri = TextBox_URI.Text;
            ProgramCore.CurUserSettings.Email = TextBox_Email.Text;
            ProgramCore.CurUserSettings.Apikey = TextBox_APIKey.Text;
            ProgramCore.CurUserSettings.Password = TextBox_Password.Text;
            this.Hide();
            this.Dispose();

            UserSettings.Save(ProgramCore.CurUserSettings);
            ProgramCore.UpdateHotkeys();
        }

        /// <summary>
        /// JPEG button check function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {

            if (radioButton5.Checked)
                ProgramCore.CurUserSettings.ImgFormat = ImageFormat.Jpeg;
        }

        /// <summary>
        /// Png button check function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

            if (radioButton1.Checked)
                ProgramCore.CurUserSettings.ImgFormat = ImageFormat.Png;
        }

        /// <summary>
        /// Gif button check function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

            if (radioButton2.Checked)
                ProgramCore.CurUserSettings.ImgFormat = ImageFormat.Gif;
        }

        /// <summary>
        /// tiff button check function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

            if (radioButton3.Checked)
                ProgramCore.CurUserSettings.ImgFormat = ImageFormat.Tiff;
        }

        private void DropDown_FullScreenMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (startup) return;
            Debug.WriteLine(ProgramCore.CurUserSettings.FullCaptureMode);
            ProgramCore.CurUserSettings.FullCaptureMode = (FullScreenCaptureMode)DropDown_FullScreenMode.SelectedItem;
            Debug.WriteLine(ProgramCore.CurUserSettings.FullCaptureMode);
        }


        private void CheckBox_postProcess_CheckedChanged(object sender, EventArgs e)
        {
            ProgramCore.CurUserSettings.PostProcessArtifactRemoval = CheckBox_postProcess.Checked;
        }

        private void CheckBox_SaveToHDD_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox_SaveToHDD.Checked)
            {
                TextBox_SavePath.Enabled = true;
                Button_SelectPath.Enabled = true;
                ProgramCore.CurUserSettings.SaveToHDD = true;
            }
            else
            {
                TextBox_SavePath.Enabled = false;
                Button_SelectPath.Enabled = false;
                ProgramCore.CurUserSettings.SaveToHDD = false;
            }
        }

        private void TextBox_SavePath_TextChanged(object sender, EventArgs e)
        {
            ProgramCore.CurUserSettings.DefaultSavePath = TextBox_SavePath.Text;
        }

        private void Button_SelectPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog diag = new FolderBrowserDialog();
            DialogResult result = diag.ShowDialog();
            if (result == DialogResult.OK)
            {
                TextBox_SavePath.Text = diag.SelectedPath;
                ProgramCore.CurUserSettings.DefaultSavePath = diag.SelectedPath;
            }
        }

        private void CheckBox_AutoUpload_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox_AutoUpload.Checked)
            {
                CheckBox_ToClipboard.Enabled = true;
                ProgramCore.CurUserSettings.UploadToServer = true;
            }
            else
            {
                CheckBox_ToClipboard.Enabled = false;
                ProgramCore.CurUserSettings.UploadToServer = false;
            }
        }

        private void CheckBox_ToClipboard_CheckedChanged(object sender, EventArgs e)
        {
            ProgramCore.CurUserSettings.CopyToClipboard = CheckBox_ToClipboard.Enabled;

        }

        private void CheckBox_StartWithWindows_CheckedChanged(object sender, EventArgs e)
        {
            if (startup) return;
            ProgramCore.CurUserSettings.StartOnStartup = CheckBox_StartWithWindows.Checked;
        }

        public void ChangeTabIndex(int i)
        {
            TabControl.SelectTab(i);
        }

        public void ChangeTagByName(string tabname)
        {
            TabControl.SelectTab(tabname);
            
        }

        private void TextBox_Password_TextChanged(object sender, EventArgs e)
        {

        }

        private void CheckBox_StartInTray_CheckedChanged(object sender, EventArgs e)
        {
            if (startup) return;
            ProgramCore.CurUserSettings.StartInTray = CheckBox_StartInTray.Checked;
        }

        [System.Obsolete("John Cena has been removed")]
        private void CheckBox_JohnCena_CheckedChanged(object sender, EventArgs e)
        {
            //if (CheckBox_JohnCena.Checked)
            //{
            //    CheckBox_CustomConfirmSound.Enabled = false;
            //    TextBox_SoundPath.Enabled = false;
            //    Button_SelectSoundPath.Enabled = false;
            //    ProgramCore.CurUserSettings.JohnCena = true;
            //}
            //else
            //{
            //    CheckBox_CustomConfirmSound.Enabled = true;
            //    if(CheckBox_CustomConfirmSound.Checked)
            //        TextBox_SoundPath.Enabled = true;
            //    Button_SelectSoundPath.Enabled = true;
            //    ProgramCore.CurUserSettings.JohnCena = false;
            //}
        }

        private void CheckBox_CustomConfirmSound_CheckedChanged(object sender, EventArgs e)
        {
            if (startup) return;
            ProgramCore.CurUserSettings.UseCustomShareSound = CheckBox_CustomConfirmSound.Checked;
            if (CheckBox_CustomConfirmSound.Checked)
            {
                TextBox_SoundPath.Enabled = true;
            }
            else
            {
                TextBox_SoundPath.Enabled = false;
            }
        }

        private void TextBox_SoundPath_TextChanged(object sender, EventArgs e)
        {
            if (startup) return;
            ProgramCore.CurUserSettings.CustomShareSoundPath = TextBox_SoundPath.Text;
        }

        private void Button_SelectSoundPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();
            diag.Filter = "Sound Files (*.wav) |*.wav";
            
            DialogResult result = diag.ShowDialog();
            if (result == DialogResult.OK)
            {
                TextBox_SoundPath.Text = diag.FileName;
                ProgramCore.CurUserSettings.CustomShareSoundPath = diag.FileName;
                if (diag.CheckFileExists)
                {
                    ProgramCore.CustomDoneSound = new System.Media.SoundPlayer(diag.FileName);
                }
            }
        }

        private void MasterSettings_Load(object sender, EventArgs e)
        {

        }

        private void TrackBar_WebmQuality_Scroll(object sender, EventArgs e)
        {
            if (startup) return;
            ProgramCore.CurUserSettings.WebMQuality = TrackBar_WebmQuality.Value;
        }

        private void TrackBar_Webm_Framerate_Scroll(object sender, EventArgs e)
        {
            if (startup) return;
            Label_WebM_CurrentFPS.Text = TrackBar_Webm_Framerate.Value.ToString();
            ProgramCore.CurUserSettings.WebMFramerate = TrackBar_Webm_Framerate.Value;
        }

        private void CheckBox_WebM_RecCursor_CheckedChanged(object sender, EventArgs e)
        {
            if (startup) return;
            ProgramCore.CurUserSettings.WebMrecCursor = CheckBox_WebM_RecCursor.Checked;
        }

        delegate void ChangeTextCallback(Label l, string input);

        public void ChangeLabelText(Label l, string input)
        {

            try
            {
                if (this.InvokeRequired)
                {
                    ChangeTextCallback cb = new ChangeTextCallback(ChangeLabelText);
                    this.Invoke(cb, new object[] { l, input});
                }
                else
                {
                    l.Text = input;
                }
            }
            // most likely the programm got terminated at this point
            catch { }

        }
    
        /// <summary>
        /// Starts a key monitoring process, writes to a variable constantly and can change the text of a label
        /// </summary>
        /// <param name="settingsBuffer"></param>
        /// <param name="labelToChange"></param>
        public void MonitorKeyForAction(ShareDatAction action,Label labelToChange)
        {
            
            HotkeyManager.CurrentMonitorMode = HotkeyManager.MonitorMode.Record;
            labelToChange.ForeColor = Color.Red;
            t_keyMonitor = new Thread(()=> {
                bool inputStarted = false;
                while (true)
                {
                    Thread.Sleep(200);
                    if (!HotkeyManager.AnyKeyPressed() && inputStarted)
                    {
                        labelToChange.ForeColor = Color.Black;

                        t_keyMonitor.Abort();
                    }
                    if (HotkeyManager.AnyKeyPressed() && !inputStarted)
                    {
                        inputStarted = true;
                    }

                    HotkeyData usedbuffer = new HotkeyData(null,null);
                   

                    usedbuffer = HotkeyManager.GetCurrentlyHeldHotkeyData();
                    AssignHotkeyTo(action, usedbuffer);
                    ChangeLabelText(labelToChange, usedbuffer.Name);
                    HotkeyManager.CurrentMonitorMode = HotkeyManager.MonitorMode.Monitor;
                }

            });
            t_keyMonitor.Start();
        }

        public void AssignHotkeyTo(ShareDatAction action, HotkeyData hotkeyData)
        {
            switch (action)
            {
                case (ShareDatAction.AreaScreenshot):
                     ProgramCore.CurUserSettings.HotkeySettings_AreaScreen = hotkeyData;
                    break;
                case (ShareDatAction.FullScreenshot):
                    ProgramCore.CurUserSettings.HotkeySettings_FullScreen = hotkeyData;
                    break;
                case (ShareDatAction.ClipboardUpload):
                    ProgramCore.CurUserSettings.HotkeySettings_UploadClipBoard = hotkeyData;
                    break;
                case (ShareDatAction.AreaWebmRecord):
                    ProgramCore.CurUserSettings.HotkeySettings_WebmRecord = hotkeyData;
                    break;
                case (ShareDatAction.AreaWebmRecordStop):
                    ProgramCore.CurUserSettings.HotkeySettings_WebmRecordStop = hotkeyData;
                    break;
            }
        }

        Thread t_keyMonitor;

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void Button_HKey_ScreenCap_Click(object sender, EventArgs e)
        {
            MonitorKeyForAction(ShareDatAction.FullScreenshot, Label_HKey_ScreenCap);
        }

        private void Button_HKey_AreaCapture_Click(object sender, EventArgs e)
        {
            MonitorKeyForAction(ShareDatAction.AreaScreenshot, Label_HKey_AreaCapture);

        }

        private void Button_HKey_WebMCapture_Click(object sender, EventArgs e)
        {
            MonitorKeyForAction(ShareDatAction.AreaWebmRecord, Label_HKey_WebMCapture);

        }

        private void Button_HKey_WebMCaptureStop_Click(object sender, EventArgs e)
        {
            MonitorKeyForAction(ShareDatAction.AreaWebmRecordStop, Label_HKey_WebMCaptureStop);

        }

        private void Button_HKey_UploadClip_Click(object sender, EventArgs e)
        {
            MonitorKeyForAction(ShareDatAction.ClipboardUpload, Label_HKey_UploadClip);

        }

        private void DropDown_NotifiMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (startup) return;
            Debug.WriteLine(ProgramCore.CurUserSettings.FullCaptureMode);
            ProgramCore.CurUserSettings.NotificationMode = (NotificationMode)DropDown_NotifiMode.SelectedItem;
            Debug.WriteLine(ProgramCore.CurUserSettings.FullCaptureMode);
        }
    }
}
