﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AImageTool.Forms
{
    public partial class UploadMonitor : Form
    {
        public UploadMonitor()
        {
            var t = Task.Run(async delegate
            {
                bool running = true;
                while (running) {
                    await Task.Delay(1000);
                    if(ProgramCore.ProgressMonitor != this)
                    {
                        KillDelayed();
                        running = false;
                    }
                }
                

            });
            InitializeComponent();
        }

        private void UploadMonitor_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Width, Screen.PrimaryScreen.Bounds.Top);
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 100;
            ShowInTaskbar = false;

        }

        public void MonitorVideoConversionProgress(object sender, NReco.VideoConverter.ConvertProgressEventArgs args)
        {
            Console.WriteLine("Called!!");
            if (this.InvokeRequired)
            {
                VideoHandler.ProgressCallback cb = new VideoHandler.ProgressCallback(MonitorVideoConversionProgress);
                this.Invoke(cb, new object[] { sender, args });
            }
            else
            {
                int t = (int)(((float)args.Processed.TotalMilliseconds / args.TotalDuration.TotalMilliseconds) * 100);

                if (t <= 100)
                    progressBar1.Value = t;
            }
        }

        public void MonitorUploadProgress(float progress)
        {
            if (this.InvokeRequired)
            {
                ServerConnector.ProgressCallback cb = new ServerConnector.ProgressCallback(MonitorUploadProgress);
                this.Invoke(cb, new object[] { progress });
            }
            else
            {
                progressBar1.Value = (int)(progress * 100);
            }
        }

        public delegate void ChangeInt(int pct);
        public void ChangePercentage(int pct)
        {
            if (this.InvokeRequired)
            {
                ChangeInt cb = new ChangeInt(ChangePercentage);
                this.Invoke(cb, new object[] { pct });
            }
            else
            {
                progressBar1.Value = pct;
            }
        }

        public delegate void ChangeStr(string str);
        public void ChangeTaskText(string text)
        {
            if (this.InvokeRequired)
            {
                ChangeStr cb = new ChangeStr(ChangeTaskText);
                this.Invoke(cb, new object[] { text });
            }
            else
            {
                label2.Text = text;
            }
        }

        public delegate void VoidDelegate();
        public void KillDelayed()
        {
            if (this.InvokeRequired)
            {
                VoidDelegate cb = new VoidDelegate(KillDelayed);
                this.Invoke(cb);
            }
            else
            {
                var t = Task.Run(async delegate
                {
                    await Task.Delay(TimeSpan.FromSeconds(4));
                    KillSelf();
                    
                });
            }
        }

        public void KillSelf()
        {
            if (this.InvokeRequired)
            {
                VoidDelegate cb = new VoidDelegate(KillSelf);
                this.Invoke(cb);
            }
            else
            {
                this.Close();
                if (ProgramCore.ProgressMonitor == this)
                    ProgramCore.ProgressMonitor = null;
                this.Dispose();
            }

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }
    }
}
