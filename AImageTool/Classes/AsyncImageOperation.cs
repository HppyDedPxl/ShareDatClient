﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AImageTool
{
    public class AsyncImageOperation
    {
        public static float Progress = 0;
        public static bool isUploading = false;
        public static bool isDone = false;
        public static string newClipboardContent = "";
        public static string Lasterror = "";
        public static string Status = "";
        public static string StandardStatus = "SharedDat Alpha.160504";
        public static string ToDeleteFilePath = "";

        public static void Reset()
        {
            Progress = 0;
            isDone = false;
            newClipboardContent = "";
            Lasterror = "";
            Status = StandardStatus;
            isUploading = false;
        }
    }
}
