﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AImageTool
{
    internal struct BitmapAndBounds
    {
        public Bitmap bitmap;
        public Rectangle bounds;
    }

    internal class ImageHandler
    {
        // link CurrentImageFormat with the main programms user settings
        public static ImageFormat CurrentImageFormat
        {
            get
            {
                return ProgramCore.CurUserSettings.ImgFormat;
            }

            set
            {
                ProgramCore.CurUserSettings.ImgFormat = value;
            }
        }

        /// <summary>
        /// Creates a Full Screenshot
        /// </summary>
        /// <returns></returns>
        internal Bitmap CreateScreenshotFull(FullScreenCaptureMode mode)
        {
            Bitmap final = null;
            if (mode == FullScreenCaptureMode.All)
            {
                final = CreateScreenshotAllMonitors();
            }
            else if (mode == FullScreenCaptureMode.Mouse)
            {
                final = CreateScreenshotMouseMonitor();
            }
            else
            {
                final = CreateScreenshotPrimaryMonitor();
            }
            return final;

        }

        /// <summary>
        /// Creates a screenshot of all monitors and returns the bitmap
        /// </summary>
        /// <returns></returns>
        internal Bitmap CreateScreenshotAllMonitors()
        {
            List<BitmapAndBounds> bmps = new List<BitmapAndBounds>();

            for (int i = 0; i < Screen.AllScreens.Length; i++)
            {

                Bitmap bmp = CreateScreenshotOfScreen(Screen.AllScreens[i]);
                bmps.Add(new BitmapAndBounds() { bitmap = bmp, bounds = Screen.AllScreens[i].Bounds});
            }
            return CombineBitmap(bmps);
        }

        /// <summary>
        /// Creates a screenshot of the primary monitor and returns the bitmap
        /// </summary>
        /// <returns></returns>
        internal Bitmap CreateScreenshotPrimaryMonitor()
        {
            return CreateScreenshotOfScreen(Screen.PrimaryScreen); 
        }

        /// <summary>
        /// Creates a screenshot of the monitor the mouse is on and returns the bitmap
        /// </summary>
        /// <returns></returns>
        internal Bitmap CreateScreenshotMouseMonitor()
        {
            Screen curScreen = GetScreenWithMouse();
            return CreateScreenshotOfScreen(curScreen);

        }

        /// <summary>
        /// Creates a screenshot of a given screen and returns the bitmap
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        internal Bitmap CreateScreenshotOfScreen(Screen screen)
        {
            Bitmap bmp = new Bitmap(screen.Bounds.Width, screen.Bounds.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Rectangle rect = new Rectangle(screen.Bounds.X, screen.Bounds.Y, screen.Bounds.Width, screen.Bounds.Height);

            using (Graphics graphic = Graphics.FromImage(bmp))
            {
                graphic.CopyFromScreen(rect.Left, rect.Top, 0, 0, bmp.Size, CopyPixelOperation.SourceCopy);
            }
            return bmp;
        }

        /// <summary>
        /// Creates a screenshot of the provided screencoordinate rectangle and returns the bitmap
        /// </summary>
        /// <param name="x0">upper left point x</param>
        /// <param name="y0">upper left point y</param>
        /// <param name="x1">lower right point x</param>
        /// <param name="y1">lower right point y</param>
        /// <returns></returns>
        internal Bitmap CreateScreenshotPartial(int x0, int y0, int x1, int y1)
        {

            Rectangle rect = new Rectangle(x0, y0, x1, y1);

            Bitmap bmp = new Bitmap(rect.Width, rect.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            using (Graphics graphic = Graphics.FromImage(bmp))
            {
                graphic.CopyFromScreen(rect.Left,rect.Top, 0, 0, bmp.Size, CopyPixelOperation.SourceCopy);              
            }

            return bmp;
        }

        /// <summary>
        /// Creates a screenshot of the provided screen rectangle and returns the bitmap
        /// </summary>
        /// <param name="rectangle"></param>
        /// <returns></returns>
        internal Bitmap CreateScreenshotPartial(Rectangle rectangle)
        {
            // reform rectangle
            Rectangle rect = new Rectangle(rectangle.X, rectangle.Y, rectangle.Width,  rectangle.Height);
            Bitmap bmp;
            try {
                bmp = new Bitmap(rect.Width, rect.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                using (Graphics graphic = Graphics.FromImage(bmp))
                {
                    graphic.CopyFromScreen(rect.Left, rect.Top, 0, 0, bmp.Size, CopyPixelOperation.SourceCopy);
                }
            }
            catch(ArgumentException e)
            {
                return null;
            }
            return bmp;
        }

        /// <summary>
        /// Save the Bitmap with a unique name to the specified directory
        /// The image format is driven by this classes static value settings
        /// </summary>
        /// <param name="bmp"></param>
        /// <param name="format"></param>
        internal void SaveBitmap(Bitmap bmp,string path)
        {
            bmp.Save(path, CurrentImageFormat);
        }

        /// <summary>
        /// Combine a collection of Bitmaps, sorting them according to their bounds (So that the order of monitors is preserved on the screenshot)
        /// </summary>
        /// <param name="images"></param>
        /// <returns>Combined image</returns>
        private static Bitmap CombineBitmap(ICollection<BitmapAndBounds> data)
        {
            // if we only have one image, return that
            if (data.Count == 1)
            {
                return data.First().bitmap;
            }

            // sort bitmaps from left to right
            List<BitmapAndBounds> sortedData = data.OrderBy(o => o.bounds.X).ToList();

            // declare result image
            Bitmap result = null;

            // get final width and height of our image
            int width = 0;
            int height = 0;

            for (int i = 0; i < sortedData.Count; i++)
            {
                width += sortedData[i].bitmap.Width;
                height = sortedData[i].bounds.Height > height ? sortedData[i].bounds.Height : height;
            }

            // create empty
            result = new Bitmap(width, height);

            using (Graphics g = Graphics.FromImage(result))
            {
                g.Clear(System.Drawing.Color.Black);
                int offset = 0;
                for (int i = 0; i < sortedData.Count; i++)
                {
                    g.DrawImage(sortedData[i].bitmap,
                        new Rectangle(offset, 0, sortedData[i].bitmap.Width, sortedData[i].bitmap.Height));
                    offset += Math.Abs(sortedData[i].bounds.Width);
                }
            }

            return result;
        }

        /// <summary>
        /// Sometimes artifacts occur when capturing screen data. (Alpha 0 pixels).
        /// This method removes those pixels and replaces them with a more appropriate color
        /// todo: neighbour sensitive interpolation
        /// </summary>
        /// <param name="bmp"></param>
        /// <returns>Postprocessed pixels</returns>
        [System.Obsolete("This is an older, slower version of the algorithm. Use PostProcessArtifactRemovalFast(Bitmap bmp) instead for ~ 60% more speed.")]
        public Bitmap PostProcessArtifactRemoval(Bitmap bmp)
        {
            for (int x = 0; x < bmp.Size.Width; x++)
            {
                for (int y = 0; y < bmp.Size.Height; y++)
                {
                    System.Drawing.Color c = bmp.GetPixel(x, y);
                    if (c.A == 0)
                        bmp.SetPixel(x, y, System.Drawing.Color.Black);
                }
            }
            return bmp;
        }

        /// <summary>
        /// Sometimes artifacts occur when capturing screen data. (Alpha 0 pixels).
        /// This method removes those pixels and replaces them with a more appropriate color
        /// todo: neighbour sensitive interpolation
        /// </summary>
        /// <param name="bmp"></param>
        /// <returns>Postprocessed pixels</returns>
        public Bitmap PostProcessArtifactRemovalFast(Bitmap bmp)
        {
            // lock bits to data struct
            BitmapData bmpData = bmp.LockBits(new Rectangle(new Point(), bmp.Size), ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            // needed pixel amount
            int pixelCount = bmp.Height * bmp.Width;
            // bytes per pixel
            int bpp = System.Drawing.Bitmap.GetPixelFormatSize(System.Drawing.Imaging.PixelFormat.Format32bppArgb) / 8;

            // allocate space for raw bitmap data            
            byte[] bitmapBytes;
            bitmapBytes = new byte[pixelCount * bpp];

            // copy all data from the head of the image pointer till the end of the image
            Marshal.Copy(bmpData.Scan0, bitmapBytes, 0, bitmapBytes.Length);

            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    // start index of pixel (r value)
                    int i = ((y * bmp.Width) + x) * bpp;

                    // check for transparency
                    if (bitmapBytes[i + 3] == 0)
                    {
                        // handle interpolation here, for now set to black.
                        bitmapBytes[i] = 0;
                        bitmapBytes[i + 1] = 0;
                        bitmapBytes[i + 2] = 0;
                        bitmapBytes[i + 3] = 255;
                    }
                }
            }

            // copy back byte data to the actual image
            Marshal.Copy(bitmapBytes, 0, bmpData.Scan0, bitmapBytes.Length);
            // unlock bits again
            bmp.UnlockBits(bmpData);

            return bmp;
        }

        /// <summary>
        /// Converts a bitmap into raw color data in byte form
        /// </summary>
        /// <param name="bmp"></param>
        /// <returns></returns>
        public static byte[] BitmapToByte(Bitmap bmp)
        {
            // lock bits to data struct
            BitmapData bmpData = bmp.LockBits(new Rectangle(new Point(), bmp.Size), ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            // needed pixel amount
            int pixelCount = bmp.Height * bmp.Width;
            // bytes per pixel
            int bpp = System.Drawing.Bitmap.GetPixelFormatSize(System.Drawing.Imaging.PixelFormat.Format32bppArgb) / 8;

            // allocate space for raw bitmap data            
            byte[] bitmapBytes;
            bitmapBytes = new byte[pixelCount * bpp];

            // copy all data from the head of the image pointer till the end of the image
            Marshal.Copy(bmpData.Scan0, bitmapBytes, 0, bitmapBytes.Length);

            // unlock bitmap
            bmp.UnlockBits(bmpData);

            return bitmapBytes;
        }

        public static byte[] BitmapToByte2(Bitmap bmp)
        {
            // Special case just to be able to set JPEG quality a bit higher... Sigh...
            if (CurrentImageFormat == ImageFormat.Jpeg)
            {

                // create a new encode object for quality
                System.Drawing.Imaging.Encoder enc = System.Drawing.Imaging.Encoder.Quality;
                // create a new encoder parameters object
                System.Drawing.Imaging.EncoderParameters paramss = new System.Drawing.Imaging.EncoderParameters(1);
                // with the encoder, create a quality parameter
                System.Drawing.Imaging.EncoderParameter param = new EncoderParameter(enc, 250L);
                // set it to the parameter array
                paramss.Param[0] = param;

                // get desired codec info.
                ImageCodecInfo myImageCodecInfo = null;
            
                int j;
                ImageCodecInfo[] encoders;
                encoders = ImageCodecInfo.GetImageEncoders();
                for (j = 0; j < encoders.Length; ++j)
                {
                    if (encoders[j].MimeType == "image/jpeg")
                        myImageCodecInfo = encoders[j];
                }
               

                // save
                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, myImageCodecInfo ,paramss);
                return ms.GetBuffer();
            }
            // otherwise just go with the default values
            else
            {
                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, CurrentImageFormat);
                return ms.GetBuffer();
            }
           
            

            
        }

        /// <summary>
        /// Returns the screen the mouse cursor is currently on
        /// </summary>
        /// <returns></returns>
        private Screen GetScreenWithMouse()
        {
            for (int i = 0; i < Screen.AllScreens.Length; i++)
            {
                if (IsPointInBoundary(Screen.AllScreens[i].Bounds.X,
                    Screen.AllScreens[i].Bounds.Y,
                    Screen.AllScreens[i].Bounds.Width,
                    Screen.AllScreens[i].Bounds.Height,
                    Cursor.Position.X,
                    Cursor.Position.Y
                    ))
                {
                    return Screen.AllScreens[i];
                }
            }
            return null;
        }

        /// <summary>
        /// Returns true if a point p is in the boundaries of a rectangle
        /// </summary>
        /// <param name="rX">upper left corner of rect (X component)</param>
        /// <param name="rY">upper left corner of rect (X component)</param>
        /// <param name="rW">Width of the rectangle</param>
        /// <param name="rH">Height of the rectangle</param>
        /// <param name="pX">point X Coordinate</param>
        /// <param name="pY">point Y Coordinate</param>
        /// <returns></returns>
        private bool IsPointInBoundary(int rX, int rY, int rW, int rH, int pX, int pY)
        {
            return (pX <= rX + rW && pX >= rX && pY < rY + rH && pY >= rY);
        }

    }



}



