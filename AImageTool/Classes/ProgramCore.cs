﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using System.IO;
using System.Collections.Specialized;
using System.Media;
using Screna;
using Screna.Avi;

namespace AImageTool
{

    public class ProgramCore
    {

        private static ProgramCore instance;
        // Reference to the current viewDrawer for selection and overlay management
        private ViewDrawer curViewDrawer;
        // Reference to the current ImageHandler, should always be there
        private ImageHandler iHandler;
        // Reference to the current VideoHandler, should always be there
        private VideoHandler vHandler;
        // Reference to the current FileHandler, should always be there
        private FileHandler fHandler;
        // Reference to the parent UI
        private MainGUI parentForm;
        // Reference to the hotkeymanager
        private static HotkeyManager hotkeyManager;
        // Reference to the server connector
        private ServerConnector serverConnector;
        // current user Settings
        private static UserSettings curUserSettings;

        internal static UserSettings CurUserSettings
        {
            get
            {
                if (curUserSettings != null)
                    return curUserSettings;
                else
                {
                    curUserSettings = UserSettings.Load();
                    return curUserSettings;
                }
            }

            set
            {
                curUserSettings = value;
                UserSettings.Save(curUserSettings);
            }
        }

        internal static SoundPlayer CustomDoneSound
        {
            get
            {
                return customDoneSound;
            }

            set
            {
                customDoneSound = value;
            }
        }

        internal static HotkeyManager HotkeyManager
        {
            get
            {
                return hotkeyManager;
            }
        }

        private static SoundPlayer customDoneSound;

        internal int privilegeLevel = 1;

        public string lastUrl = "";

        private int currentIconIndex;
        // can hold a visual indicator form (e.g. a rect that indicates recording areas) that still has to be destroyed upon a process being stopped
        private Form VisualIndicatorToDestroy;

        public static Forms.UploadMonitor ProgressMonitor;

        Thread t_MonitorUploadOperation;

        /// <summary>
        /// Programm constructor and internal start
        /// </summary>
        /// <param name="form"></param>
        public ProgramCore(MainGUI form)
        {
            // Store reference to parent form
            parentForm = form;
            // Initialize a new ImageHandler object
            iHandler = new ImageHandler();
            //Initialize a new VideoHandler object
            vHandler = new VideoHandler();
            // Initialize a new FileHandler object
            fHandler = new FileHandler();
            //Initialize Hotkeys
            DefineHotkeys();
            // Initialize server connector
            serverConnector = new ServerConnector();
            // load userSettings
            curUserSettings = UserSettings.Load();
            // self reference for static calls
            instance = this;

            // Privilege level currently not working server sided.
            // privilegeLevel = GetUserPrivileges();   

            #region UploadMonitor Thread
            t_MonitorUploadOperation = new Thread(
                () =>
                {
                    while (true)
                    {

                        if (AsyncImageOperation.isDone)
                        {
                            if (CurUserSettings.UploadToServer && CurUserSettings.CopyToClipboard)
                            {
                                System.Windows.Clipboard.SetText(AsyncImageOperation.newClipboardContent);
                                lastUrl = AsyncImageOperation.newClipboardContent;
                            }

                            if(CurUserSettings.NotificationMode == NotificationMode.WindowsNotifications)
                                parentForm.ShowNotificationFromTray("Done!", true,true);
                            else
                            {
                                if(ProgressMonitor != null)
                                {
                                    ProgressMonitor.ChangePercentage(100);
                                    ProgressMonitor.ChangeTaskText("Done!");
                                    ProgressMonitor.KillDelayed();
                                    parentForm.PlayPositiveSound();
                                }
                            }

                            Debug.WriteLine("Operations Done...");
                            // reset icon
                            parentForm.ChangeIcon(Properties.Resources.sharedat);
                            Debug.WriteLine(AsyncImageOperation.newClipboardContent);
                            AsyncImageOperation.Reset();
                            currentIconIndex = 0;
                        }
                        // Update loading icon otherwise

                        if(AsyncImageOperation.isUploading)
                        {
                            int cr = Math.Abs((currentIconIndex++ % 4)-2);
                            Icon ic = null;
                            switch (cr)
                            {
                                case (0):
                                    ic = Properties.Resources.sharedat;
                                    break;
                                case (1):
                                    ic = Properties.Resources.sharedat_loading1;
                                    break;
                                case (2):
                                    ic = Properties.Resources.sharedat_loading2;
                                    break;
                                default:
                                    ic = Properties.Resources.sharedat;
                                    break;
                            }
                            parentForm.ChangeIcon(ic);
                        }
                        Thread.Sleep(150);
                    }
                }
                );
            t_MonitorUploadOperation.SetApartmentState(ApartmentState.STA);
            t_MonitorUploadOperation.Start();

            #endregion

            if (File.Exists(curUserSettings.CustomShareSoundPath))
            {
                CustomDoneSound = new SoundPlayer(curUserSettings.CustomShareSoundPath);
            } else{
                curUserSettings.CustomShareSoundPath = "";
                curUserSettings.UseCustomShareSound = false;
            }

            System.Windows.Forms.Application.ApplicationExit += new EventHandler(this.OnApplicationExit);
        }

        void OnApplicationExit(object sender, EventArgs e)
        {
           
           t_MonitorUploadOperation.Abort();
        }

        ~ProgramCore()
        {
            if(t_MonitorUploadOperation != null)
                t_MonitorUploadOperation.Abort();
        }

        public static void UpdateHotkeys()
        {
            if (instance != null)
                instance.DefineHotkeys();
        }

        /// <summary>
        /// Defines all necessary hotkeys
        /// </summary>
        public void DefineHotkeys()
        {
            if (hotkeyManager == null) {
                hotkeyManager = new HotkeyManager();
                hotkeyManager.Init();
            }

            hotkeyManager.WipeHotkeys();


            // Full All screen capture
            hotkeyManager.RegisterHotkey(CurUserSettings.HotkeySettings_FullScreen, () =>
            {
                if (CheckForValidSettings()) 
                    CreateFullScreenScreenshot();
                
            });

            hotkeyManager.RegisterHotkey(CurUserSettings.HotkeySettings_WebmRecord, () =>
            {
                if (CheckForValidSettings() && !vHandler.isRecording())
                {
                    InitializeAreaRecording();
                }
            });


            hotkeyManager.RegisterHotkey(CurUserSettings.HotkeySettings_WebmRecordStop, () =>
            {
                StopAreaRecording();
            });

            // Region capture
            hotkeyManager.RegisterHotkey(CurUserSettings.HotkeySettings_AreaScreen, () =>
            {
                if (CheckForValidSettings())
                    CreateAreaScreenshot();
            });

            //hotkeyManager.RegisterHotkey(new int[] { HotkeyManager.WM_CTRL, HotkeyManager.WM_SHIFT }, Keys.D5.GetHashCode(), () =>
            //{
            //    if (CheckForValidSettings())
            //        UploadSelectedFiles();
            //});

            hotkeyManager.RegisterHotkey(CurUserSettings.HotkeySettings_UploadClipBoard, () =>
            {
                if (CheckForValidSettings())
                    UploadClipboard();
            });



        }

        private bool CheckForValidSettings()
        {
            if (!CurUserSettings.UploadToServer) return true;
            if(CurUserSettings.Uri == "" || CurUserSettings.Email == "" || CurUserSettings.Password == "" || CurUserSettings.Apikey == "")
            {
                System.Windows.Forms.MessageBox.Show("Some connection data is missing. Please fill in all the necessary information!", "Cheesy Errors! NOO!", MessageBoxButtons.OK);
                return false;
            }
            return true;
        }

        private bool CheckForValidSavePaths()
        {
            if (ProgramCore.CurUserSettings.SaveToHDD)
            {
                if (!System.IO.Directory.Exists(ProgramCore.CurUserSettings.DefaultSavePath))
                {
                    System.Windows.Forms.MessageBox.Show("Saving To HDD is enabled but the specified Path does not exist!", "Cheesy Errors! NOO!", MessageBoxButtons.OK);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Creates a screenshot of all screens and handles saving, uploading etc.
        /// </summary>
        private void CreateFullScreenScreenshot()
        {
            if (CheckForValidSavePaths())
            {
                if (CurUserSettings.NotificationMode == NotificationMode.ProgressBars)
                {
                    ShowUploadMonitor();
                }
                HandleFinalImage(iHandler.CreateScreenshotFull(ProgramCore.CurUserSettings.FullCaptureMode));
            }
           
        }

        /// <summary>
        /// Initializes the taking of a partial screenshot by initializing the area drawer and setting up callbacks
        /// </summary>
        private void CreateAreaScreenshot()
        {
            if (CheckForValidSavePaths())
            {
                if (curViewDrawer != null)
                {
                    curViewDrawer.AbortSelection();
                    curViewDrawer.FinalizeScreenDrawing();
                    curViewDrawer = null;
                }

                curViewDrawer = new ViewDrawer();
                curViewDrawer.InitializeScreenDrawing(ScreenSelectionDone, ScreenSelectionAborted);
            }
            
        }

        private void InitializeAreaRecording()
        {
            if (CheckForValidSavePaths() )
            {
                if (curViewDrawer != null)
                {
                    curViewDrawer.AbortSelection();
                    curViewDrawer.FinalizeScreenDrawing();
                    curViewDrawer = null;
                }

                curViewDrawer = new ViewDrawer();
                curViewDrawer.InitializeRecordSelection(ScreenSelectionRecordDone, ScreenSelectionRecordAborted);
            }
        }

        private void StopAreaRecording()
        {
            curViewDrawer.FinalizeScreenDrawing();
            if (VisualIndicatorToDestroy != null)
            {
                curViewDrawer.StopPermanentFront();
                VisualIndicatorToDestroy.Hide();
                VisualIndicatorToDestroy.Dispose();
                VisualIndicatorToDestroy = null;
            }
            if (vHandler != null && vHandler.isRecording())
            {
                string filepath = vHandler.StopRecording();
                HandleFinalVideo(filepath);
            }
        }

        /// <summary>
        /// Handles what will happen to a captured image (saving, uploading, etc.)
        /// </summary>
        /// <param name="bmp"></param>
        private void HandleFinalImage(Bitmap bmp)
        {
            AsyncImageOperation.Reset();
            AsyncImageOperation.isUploading = true;

            if (t_FinalizeImageWorker != null &&  t_FinalizeImageWorker.IsAlive)
            {
                t_FinalizeImageWorker.Abort();
                t_FinalizeImageWorker = null;
            }
            t_FinalizeImageWorker = new Thread(()=> { T_HandleFinalImage(bmp); });
            t_FinalizeImageWorker.Start();     
            // add server upload features etc.
        }

        /// <summary>
        /// Hanldes what will happen to a captured video
        /// </summary>
        /// <param name="filepath"></param>
        private void HandleFinalVideo(string filepath)
        {
            AsyncImageOperation.Reset();
            AsyncImageOperation.isUploading = true;

            if (t_FinalizeImageWorker != null && t_FinalizeImageWorker.IsAlive)
            {
                t_FinalizeImageWorker.Abort();
                t_FinalizeImageWorker = null;
            }
            // Prepare upload monitor
            if(CurUserSettings.NotificationMode == NotificationMode.ProgressBars)
                ShowUploadMonitor();
            t_FinalizeImageWorker = new Thread(() => { T_HandleFinalVideo(filepath); });
            t_FinalizeImageWorker.Start();
            // add server upload features etc.
        }
        
        /// <summary>
        /// Executes an action after a delay using a worker thread
        /// </summary>
        /// <param name="action"></param>
        /// <param name="ms">time in milliseconds</param>
        public void DoDelayed(Action action,int ms)
        {
            Thread t_worker = new Thread(() =>
            {
                Thread.Sleep(ms);
                action.Invoke();
            });
            t_worker.Start();
        }

        private void T_Delay(Action action,int ms)
        {
            Thread.Sleep(ms);
            action.Invoke();
        }

        Thread t_FinalizeImageWorker;
        /// <summary>
        /// Thread method for saving / uploading etc the final image
        /// </summary>
        /// <param name="bmp"></param>
        private void T_HandleFinalImage(Bitmap bmp)
        {
            Thread.Sleep(100);

            // get the fileextension from the current Imageformat
            string extension = ImageCodecInfo.GetImageEncoders().FirstOrDefault(x => x.FormatID == CurUserSettings.ImgFormat.Guid).FilenameExtension.Split(';')[0].ToLower();

            // get a unique Filename and path based on the systemclock and a guid
            string newFilename = "img_" + DateTime.Now.ToString("MMdd") + "_" + DateTime.Now.ToString("HHmmss") + "_" +  DateTime.Now.ToString("ms") + extension.Substring(1);

            // If wanted, post process the image
            if (CurUserSettings.PostProcessArtifactRemoval)
                bmp = iHandler.PostProcessArtifactRemovalFast(bmp);

            // If wanted, save the image to HDD
            if (CurUserSettings.SaveToHDD)
            {
                iHandler.SaveBitmap(bmp, CurUserSettings.DefaultSavePath + "/" + newFilename);

            }
            // Set status to Done if no upload is wanted
            if(!CurUserSettings.UploadToServer)
            {
                AsyncImageOperation.isDone = true;
            }
            // otherwise upload the picture to server
            else
            {
                Debug.WriteLine("Start Upload...");
                sWatch = new System.Diagnostics.Stopwatch();
                sWatch.Start();
                if (CurUserSettings.NotificationMode == NotificationMode.WindowsNotifications)
                {
                    parentForm.ShowNotificationFromTray("Uploading...");
                    try
                    {
                        serverConnector.UploadBytesToServer(new Uri(ProgramCore.CurUserSettings.Uri), ImageHandler.BitmapToByte2(bmp), newFilename, ProgramCore.CurUserSettings.Apikey, ProgramCore.CurUserSettings.Email, ProgramCore.CurUserSettings.Password, HandleServerResponseOnUploadEnd);
                    }
                    catch (System.UriFormatException)
                    {
                        System.Windows.Forms.MessageBox.Show("The provided URI seems to be in a wrong format. Please check your connection settings.", "Cheesy Errors!! Noo!", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    try
                    {
                        serverConnector.UploadBytesToServer(new Uri(ProgramCore.CurUserSettings.Uri), ImageHandler.BitmapToByte2(bmp), newFilename, ProgramCore.CurUserSettings.Apikey, ProgramCore.CurUserSettings.Email, ProgramCore.CurUserSettings.Password, HandleServerResponseOnUploadEnd,ProgressMonitor.MonitorUploadProgress);
                    }
                    catch (System.UriFormatException)
                    {
                        System.Windows.Forms.MessageBox.Show("The provided URI seems to be in a wrong format. Please check your connection settings.", "Cheesy Errors!! Noo!", MessageBoxButtons.OK);
                    }
                }
           }

        }


        Forms.UploadMonitor ShowUploadMonitor()
        {
            ProgressMonitor = new Forms.UploadMonitor();
            ProgressMonitor.Show();
            
            return ProgressMonitor;
        }


        /// <summary>
        /// Thread method for saving / uploading etc the final video/webm
        /// </summary>
        /// <param name="bmp"></param> 
        private void T_HandleFinalVideo(string filepath)
        {
            Thread.Sleep(100);
            string oldFp = filepath;

            // Convert here
            if (CurUserSettings.NotificationMode == NotificationMode.WindowsNotifications)
                parentForm.ShowNotificationFromTray("Processing Recording...");


            if (CurUserSettings.NotificationMode == NotificationMode.ProgressBars) {
                ProgressMonitor.ChangeTaskText("Prepare Video...");
                filepath = vHandler.ConvertToWebM(filepath, ProgressMonitor.MonitorVideoConversionProgress);
            }
            else
            {
                filepath = vHandler.ConvertToWebM(filepath);
            }
            
            
            //System.IO.File.Delete(oldFp);
            string newFilename = "WebM" + DateTime.Now.ToString("MMdd") + "_" + DateTime.Now.ToString("HHmmss") + "_" + DateTime.Now.ToString("ms") + ".webm";


            // If wanted, save the video to HDD
            if (CurUserSettings.SaveToHDD)
            {
                System.IO.File.Copy(filepath, CurUserSettings.DefaultSavePath + "/" + newFilename,true);
            }
            AsyncImageOperation.ToDeleteFilePath = filepath;

            // Set status to Done if no upload is wanted
            if (!CurUserSettings.UploadToServer)
            {
                AsyncImageOperation.isDone = true;
                AsyncImageOperation.ToDeleteFilePath = "";
                System.IO.File.Delete(filepath);
            }
            else
            {
                Debug.WriteLine("Start Upload...");
                sWatch = new System.Diagnostics.Stopwatch();
                sWatch.Start();
                if (CurUserSettings.NotificationMode == NotificationMode.WindowsNotifications)
                {
                    parentForm.ShowNotificationFromTray("Uploading...");
                    try
                    {
                        serverConnector.UploadBytesToServer(new Uri(ProgramCore.CurUserSettings.Uri), fHandler.FileToBytes(filepath), newFilename, ProgramCore.CurUserSettings.Apikey, ProgramCore.CurUserSettings.Email, ProgramCore.CurUserSettings.Password, HandleServerResponseOnUploadEnd);
                    }
                    catch (System.UriFormatException)
                    {
                        System.Windows.Forms.MessageBox.Show("The provided URI seems to be in a wrong format. Please check your connection settings.", "Cheesy Errors!! Noo!", MessageBoxButtons.OK);
                    }
                }
                else if( CurUserSettings.NotificationMode == NotificationMode.ProgressBars)
                {
                    ProgressMonitor.ChangeTaskText("Uploading...");
                    try
                    {
                        serverConnector.UploadBytesToServer(new Uri(ProgramCore.CurUserSettings.Uri), fHandler.FileToBytes(filepath), newFilename, ProgramCore.CurUserSettings.Apikey, ProgramCore.CurUserSettings.Email, ProgramCore.CurUserSettings.Password, HandleServerResponseOnUploadEnd,ProgressMonitor.MonitorUploadProgress);
                    }
                    catch (System.UriFormatException)
                    {
                        System.Windows.Forms.MessageBox.Show("The provided URI seems to be in a wrong format. Please check your connection settings.", "Cheesy Errors!! Noo!", MessageBoxButtons.OK);
                    }
                }
              
            }

        }

        System.Diagnostics.Stopwatch sWatch;

        /// <summary>
        /// Callback for region selection screenshots
        /// </summary>
        /// <param name="rect"></param>
        public void ScreenSelectionDone(Rectangle rect)
        {

            curViewDrawer.FinalizeScreenDrawing();
            // Show Upload Monitor OUTSIDE of the delay thread (otherwise it would terminate with the delay task)
            if(CurUserSettings.NotificationMode == NotificationMode.ProgressBars)
            {
                ShowUploadMonitor();
            }
            // screenshot and handle 100ms delayed so the viewDrawer redraws first.
            DoDelayed(() => {
                Bitmap bmp = iHandler.CreateScreenshotPartial(rect);
                if(bmp != null)
                    HandleFinalImage(bmp);

            }, 100);
            

        }

        /// <summary>
        /// Callback for region selection aborted
        /// </summary>
        private void ScreenSelectionAborted()
        {
            curViewDrawer.FinalizeScreenDrawing();
        }

        public void ScreenSelectionRecordDone(Rectangle rect)
        {
            VisualIndicatorToDestroy = curViewDrawer.SelectionRectForm;
            curViewDrawer.FinalizeScreenDrawing();

            DoDelayed(() => {
                vHandler.StartRecording(rect);
            }, 100);
        }

        public void ScreenSelectionRecordAborted()
        {
            if (curViewDrawer.SelectionRectForm != null)
            {
                curViewDrawer.SelectionRectForm.Hide();
                curViewDrawer.SelectionRectForm.Dispose();
            }
            curViewDrawer.FinalizeScreenDrawing();
        }

        /// <summary>
        /// This method is called after an upload has been completed, handle all server responses in here!
        /// </summary>
        /// <param name="response"></param>
        public void HandleServerResponseOnUploadEnd(string response)
        {
            if(AsyncImageOperation.ToDeleteFilePath != "" && File.Exists(AsyncImageOperation.ToDeleteFilePath))
            {
                File.Delete(AsyncImageOperation.ToDeleteFilePath);
                AsyncImageOperation.ToDeleteFilePath = "";
            }


            //manage error code stuff too here.
            // responses:
            if (response == "failed: The remote server returned an error: (502) Bad Gateway.")
            {
                if (CurUserSettings.NotificationMode == NotificationMode.WindowsNotifications)
                    parentForm.ShowNotificationFromTray("Error!", true);
                System.Windows.Forms.MessageBox.Show("The provided URI does not point to a ShareDat Service host. Please check your connection settings. (502 Bad Gateway)", "Cheesy Errors! Oh No!", MessageBoxButtons.OK);
            }
            else if(response == "Wrong API-Key")
            {
                if (CurUserSettings.NotificationMode == NotificationMode.WindowsNotifications)
                    parentForm.ShowNotificationFromTray("Error!", true);
                System.Windows.Forms.MessageBox.Show("The provided API-Key is incorrect. Please check your connection settings.", "Cheesy Errors! Oh No!", MessageBoxButtons.OK);
            }
            else if (response == "No user with this email adress found." || response == "Wrong password for this email adress.")
            {
                if (CurUserSettings.NotificationMode == NotificationMode.WindowsNotifications)
                    parentForm.ShowNotificationFromTray("Error!", true);
                System.Windows.Forms.MessageBox.Show("Login credentials appear to be incorrect. Please check your connection settings.", "Cheesy Errors! Oh No!", MessageBoxButtons.OK);
            }
            else
            {
                AsyncImageOperation.newClipboardContent = response;
                AsyncImageOperation.isDone = true;
               
            }
        }

        /// <summary>
        /// Synchronously gathers the users privileges by locking the thread until a server response has been aquired
        /// </summary>
        /// <returns></returns>
        public int GetUserPrivileges()
        {
            Debug.WriteLine("Getting User privileges....");
            int[] i = { -99 };
            serverConnector.GetUserPrivilegesFromServerAsync(CurUserSettings.Uri, CurUserSettings.Password, CurUserSettings.Email, (r) =>
            {
                i[0] = r;
                Debug.WriteLine("Aquired user privileges: " + i[0]);
            });
            while (i[0] == -99) { }
            return i[0];
        }

        /// <summary>
        /// Uploads the contents of the clipboard to the server
        /// </summary>
        private void UploadClipboard()
        {
            AsyncImageOperation.isUploading = true;

            byte[] ClipData = new byte[0];
            string filename = "cp_" + DateTime.Now.ToString("MMdd") + "_" + DateTime.Now.ToString("HHmmss") + "_" + DateTime.Now.ToString("ms");

            // Clipboard contains only text. Upload it as a txt.
            if (System.Windows.Clipboard.ContainsText())
            {
                ClipData = fHandler.StringToBytes(System.Windows.Clipboard.GetText());
                filename += ".txt";
            }
            // Clipboard contains files, upload them as ...
            else if (System.Windows.Clipboard.ContainsFileDropList())
            {
                StringCollection filePaths = System.Windows.Clipboard.GetFileDropList();
                Console.WriteLine(filePaths.Count);
                // More than one file, zip and upload
                if(filePaths.Count > 1)
                {
                    ClipData = fHandler.GetZippedFilesAsBytes(filePaths);
                    filename += ".zip";

                }
                // only one file, upload as is.
                else if (filePaths.Count == 1)
                {
                    ClipData = fHandler.FileToBytes(filePaths[0]);
                    filename = filePaths[0];
                }
                else
                {
                    AsyncImageOperation.Reset();

                    return;
                }
            }
            else if (System.Windows.Forms.Clipboard.ContainsImage())
            {
                Image img = System.Windows.Forms.Clipboard.GetImage();
                Bitmap bmp = new Bitmap(img);

                HandleFinalImage(bmp);
                return;

            }


            if (t_FinalizeImageWorker != null && t_FinalizeImageWorker.IsAlive)
            {
                t_FinalizeImageWorker.Abort();
                t_FinalizeImageWorker = null;
            }

            if(CurUserSettings.NotificationMode == NotificationMode.ProgressBars)
            {
                ShowUploadMonitor();
            }
            t_FinalizeImageWorker = new Thread(() => { T_RawFilesClipboard(ClipData,filename); });
            t_FinalizeImageWorker.Start();
        }

        /// <summary>
        /// Uploads the files that are currently selected by the user in the windows explorer
        /// </summary>
        /// <param name="files"></param>
        private void UploadSelectedFiles()
        {
            AsyncImageOperation.isUploading = true;
            string filename = "cp_" + DateTime.Now.ToString("MMdd") + "_" + DateTime.Now.ToString("HHmmss") + "_" + DateTime.Now.ToString("ms");

            byte[] fileData = new byte[0];

            StringCollection files = fHandler.GetWindowsExplorerSelection();
            Debug.WriteLine(files.Count);
            
            if (files.Count > 1)
            {
                fileData = fHandler.GetZippedFilesAsBytes(files);
                filename += ".zip";
            }
            else if (files.Count == 1)
            {
                fileData = fHandler.FileToBytes(files[0]);
                filename = files[0];
            }
            else
            {
                AsyncImageOperation.Reset();
                return;
            }

            if (t_FinalizeImageWorker != null && t_FinalizeImageWorker.IsAlive)
            {
                t_FinalizeImageWorker.Abort();
                t_FinalizeImageWorker = null;
            }
            if(CurUserSettings.NotificationMode == NotificationMode.ProgressBars) {
                ShowUploadMonitor();
            }
            t_FinalizeImageWorker = new Thread(() => { T_RawFilesClipboard(fileData, filename); });
            t_FinalizeImageWorker.Start();
        }

        private void T_RawFilesClipboard(byte[] bytes, string filename)
        {
            Thread.Sleep(100);
            if (CurUserSettings.NotificationMode == NotificationMode.WindowsNotifications)
            {
                parentForm.ShowNotificationFromTray("Uploading...");
                try
                {
                    serverConnector.UploadBytesToServer(new Uri(ProgramCore.CurUserSettings.Uri), bytes, filename, ProgramCore.CurUserSettings.Apikey, ProgramCore.CurUserSettings.Email, ProgramCore.CurUserSettings.Password, HandleServerResponseOnUploadEnd);
                }
                catch (System.UriFormatException)
                {
                    System.Windows.Forms.MessageBox.Show("The provided URI seems to be in a wrong format. Please check your connection settings.", "Cheesy Errors!! Noo!", MessageBoxButtons.OK);
                }
            }
            else if(CurUserSettings.NotificationMode == NotificationMode.ProgressBars)
            {
                try
                {
                    serverConnector.UploadBytesToServer(new Uri(ProgramCore.CurUserSettings.Uri), bytes, filename, ProgramCore.CurUserSettings.Apikey, ProgramCore.CurUserSettings.Email, ProgramCore.CurUserSettings.Password, HandleServerResponseOnUploadEnd,ProgressMonitor.MonitorUploadProgress);
                }
                catch (System.UriFormatException)
                {
                    System.Windows.Forms.MessageBox.Show("The provided URI seems to be in a wrong format. Please check your connection settings.", "Cheesy Errors!! Noo!", MessageBoxButtons.OK);
                }
            }
        }

    }
    

        
}
