﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;
using Microsoft.Win32.SafeHandles;
using System.Security.AccessControl;
using Windows;
using SHDocVw;
using System.Runtime.InteropServices;

namespace AImageTool
{
    /// <summary>
    /// This class handles File to byte conversion, compression and decompression
    /// </summary>
    public class FileHandler
    {
        /// <summary>
        /// Convertts a string to bytes
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public byte[] StringToBytes(string s)
        {
            byte[] b = new byte[s.Length * sizeof(char)];
            System.Buffer.BlockCopy(s.ToCharArray(), 0, b, 0, b.Length);
            return b;
        }

        /// <summary>
        /// converts one file to bytes
        /// :not error save
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public byte[] FileToBytes(string filepath)
        {
            FileStream fs = new FileStream(filepath, FileMode.Open);
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            fs.Close();
            return buffer;
        }

        /// <summary>
        /// Create a ZipArchive from the file list
        /// </summary>
        /// <param name="paths">Collection of paths</param>
        /// <param name="fs">Filestream to be used to write temporary data</param>
        /// <returns></returns>
        private void CreateZipFromFiles(StringCollection paths, string filename)
        {

            using (MemoryStream zipStream = new MemoryStream())
            {
                using(ZipArchive zip = new ZipArchive(zipStream, ZipArchiveMode.Create, true))
                {
                    for (int i = 0; i < paths.Count; i++)
                    {
                        
                        zip.CreateEntryFromFile(paths[i], GetProbableFilenameFromPath(paths[i]));
                    }
                    zip.Dispose();
                }

                FileStream fs = new FileStream(filename, FileMode.Create);
                zipStream.Position = 0;
                zipStream.CopyTo(fs);
                fs.Close();
                zipStream.Close();

            }     
        }

        /// <summary>
        /// Returns a byte array of a zip file that contains the files specified in the string collection
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public byte[] GetZippedFilesAsBytes(StringCollection paths)
        {
            string path = Path.GetTempFileName() + "_shareDat.zip";
            CreateZipFromFiles(paths,path);
            byte[] buffer = FileToBytes(path);
            File.Delete(path);
            return buffer;

        }

        /// <summary>
        /// returns the last part of the filepath which is suspected to be the filename
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string GetProbableFilenameFromPath(string path)
        {
            string[] subs = path.Split('\\');
            string result = subs[subs.Length - 1];
            return result;
        }

        /// <summary>
        /// Returns the contents of a filestream as bytes
        /// </summary>
        /// <param name="fs">Input filestream</param>
        /// <returns>converted byte array used for file transfer</returns>
        public byte[] FileStreamToBytes(ref FileStream fs)
        {
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            return buffer;
        }

        /// <summary>
        /// Returns the current windows shell selection
        /// thanks to: http://stackoverflow.com/questions/8292953/get-current-selection-in-windowsexplorer-from-a-c-sharp-application
        /// </summary>
        /// <returns></returns>
        public StringCollection GetWindowsExplorerSelection()
        {
            IntPtr hWnd = GetForegroundWindow();
            StringCollection paths = new StringCollection();
            Shell32.Shell sh = new Shell32.Shell();
            foreach(SHDocVw.InternetExplorer window in sh.Windows())
            {
                if (window.HWND == (int)hWnd)
                {
                    Shell32.FolderItems items = ((Shell32.IShellFolderViewDual2)window.Document).SelectedItems();
                    foreach (Shell32.FolderItem item in items)
                    {
                        paths.Add(item.Path);
                    }
                }
            }
            return paths;
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

    }
    
}
