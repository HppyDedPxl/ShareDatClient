﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace AImageTool
{
    /// <summary>
    /// This class manages the drawing of all View elements outside the normal UI, e.g. desktop rectangle selection and cursors
    /// </summary>
    public class ViewDrawer
    {
        TranspDrawCanvas curTF;
        bool forceFront = false;
        Thread t_alwaysFrontOverlay;

        public Forms.SelectionRectForm SelectionRectForm
        {
            get
            {
                if (curTF == null) return null;
                return curTF.selRectForm;
            }
        }

        // make sure the thread gets destroyed
        ~ViewDrawer()
        {
            t_alwaysFrontOverlay.Abort();
        }

        public void SetPermanentFront(Forms.SelectionRectForm f)
        {
            
            t_alwaysFrontOverlay.Abort();
            t_alwaysFrontOverlay = new Thread(() => {
                while (f!=null)
                {
                    f.SetForeground();
                }
            });
            t_alwaysFrontOverlay.Start();
        }

        public void StopPermanentFront()
        {
            
            t_alwaysFrontOverlay.Abort();
        }

        /// <summary>
        /// Initializes the selection process, calls the selection done callback when the input is done
        /// </summary>
        /// <param name="selectionDoneCallback"></param>
        public void InitializeScreenDrawing(SelectionDone selectionDoneCallback, SelectionAborted selectionAbortedCallback)
        {
            // form keeps setting its location back to 0 , 0

            curTF = new TranspDrawCanvas(selectionDoneCallback, selectionAbortedCallback);
            curTF.Show();
            curTF.Location = new Point(SystemInformation.VirtualScreen.Left, SystemInformation.VirtualScreen.Top);
            curTF.Cursor = System.Windows.Forms.Cursors.Cross;
            curTF.BringToFront();
            curTF.TopMost = true;
            curTF.Activate();
            t_alwaysFrontOverlay = new Thread(() => { while (forceFront) { curTF.BringToFront(); curTF.TopMost = true;
                    curTF.Activate();
                } });
            t_alwaysFrontOverlay.Start();

        }

        /// <summary>
        /// Initializes the selection process, calls the selection done callback when the input is done
        /// </summary>
        /// <param name="selectionDoneCallback"></param>
        /// <param name="selectionAbortedCallback"></param>
        /// <returns>The border for later destruction.</returns>
        public void InitializeRecordSelection(SelectionDone selectionDoneCallback, SelectionAborted selectionAbortedCallback)
        {
            curTF = new TranspDrawCanvas(selectionDoneCallback, selectionAbortedCallback);
            curTF.RecordingMode = true;
            curTF.Show();
            curTF.Location = new Point(SystemInformation.VirtualScreen.Left, SystemInformation.VirtualScreen.Top);
            curTF.Cursor = System.Windows.Forms.Cursors.Cross;
            curTF.BringToFront();
            curTF.TopMost = true;
            curTF.Activate();
            t_alwaysFrontOverlay = new Thread(() => {
                while (forceFront)
                {
                    curTF.BringToFront(); curTF.TopMost = true;
                    curTF.Activate();
                }
            });

            t_alwaysFrontOverlay.Start();
        }

        public void DrawRectangleOnScreen(int x0, int y0, int x1, int y1)
        {

            curTF.SelectionRect = new Rectangle(x0, y0, x1 - x0, y1 - y0);
        }

        /// <summary>
        /// Cleans up and destorys the canvas form
        /// </summary>
        public void FinalizeScreenDrawing()
        {

            forceFront = false;
            t_alwaysFrontOverlay.Abort();
            
            curTF.Cursor = System.Windows.Forms.Cursors.Default;
            curTF.Hide();
            curTF.Dispose();
        }

        public void AbortSelection()
        {
            curTF.AbortSelection();
        }

        

        
        [DllImport("User32.dll")]
        static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("User32.dll")]
        static extern int ReleaseDC(IntPtr hwnd, IntPtr dc);

    }

}
