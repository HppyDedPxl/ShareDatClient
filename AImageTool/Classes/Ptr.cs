﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Reflection;
namespace AImageTool.Classes
{



    /// <summary>
    /// Generic Baseclass for a simulated pointer in C# to use value types as references when the
    /// out or ref keywords are not available or reference deep copies are enforced in the scope the class 
    /// is used.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Ptr<T>
    {

        T value;

        public T Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;
            }
        }

        /// <summary>
        /// Creates the pointer object and will optionally try to instantiate the object with a default value or by calling a 
        /// parameterless standard constructor. Will throw an error if instantiation fails (Instantiation was wanted but
        /// the provided class does not have a parameterless standard constructor)
        /// Use this constructor for classes with a default constructor or value types.
        /// </summary>
        /// <param name="instantiate">Try to instantiate object</param>
        public Ptr(bool instantiate = true)
        {
            if (!instantiate) return;

            // will set the value to the default types value (e.g. int = 0)
            value = default(T);

            // reference Type
            if (value == null)
            {
                // string is a special type and does not have an accessible constructor, so enforce
                // a standard value of '""' here.
                if (typeof(string).IsAssignableFrom(typeof(T)))
                {
                    value = (T)Convert.ChangeType("", typeof(T));
                    return;
                }
                // For everything else, try calling a standard constructor
                ConstructorInfo ctor = typeof(T).GetConstructor(new Type[] { });
                if (ctor != null)
                {
                    value = (T)Convert.ChangeType(ctor.Invoke(new object[] { }), typeof(T));
                }
                else
                {
                    throw new Exception("Could not find a default constructor with 0 arguments for Ptr<T> instance, type: " + typeof(T));
                }
            }
        }

        /// <summary>
        /// Creates the pointer object and will try to instantiate it using the provided parameters as an identification and arguments for a constructor.
        /// Use this method to create and instantiate pointers for classes without a parameterless standard constructor
        /// </summary>
        /// <param name="ctorTypes"></param>
        /// <param name="args"></param>
        public Ptr(Type[] ctorTypes, object[] args)
        {
            if (ctorTypes.Length != args.Length)
            {
                throw new Exception("Argument Type-List and argument list are incompatible (Count Mismatch)");
            }
            for (int i = 0; i < ctorTypes.Length; i++)
            {
                if (ctorTypes[i] != args[i].GetType())
                {
                    throw new Exception("Argument Type-List and argument list are incompatible (Type Mismatch)");
                }
            }

            ConstructorInfo ctor = typeof(T).GetConstructor(ctorTypes);
            if (ctor != null)
            {
                value = (T)Convert.ChangeType(ctor.Invoke(args), typeof(T));
            }
            else
            {
                throw new Exception("Could not find a default constructor with provided argumenttypes for Ptr<T> instance, type: " + typeof(T) + "\nor argument list was incompatible.");
            }
        }
    }

}


