﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;

namespace AImageTool
{
    class HotkeyManager
    {
        public enum MonitorMode
        {
            /// <summary>
            /// Monitors the current key events and invokes hotkey callbacks
            /// </summary>
            Monitor,
            /// <summary>
            /// Does not invoke any hotkey callbacks, use when e.g. setting new hotkeys
            /// </summary>
            Record
        }

        public struct Hotkey
        {
            public int[] Modifiers;
            public int HotkeyCode;
            public Action OnInvoke;
        }

        public const int WM_SHIFT = 0xa0;
        public const int WM_ALT = 0xa4;
        public const int WM_CTRL = 0xa2;

        public static int OtherPressedKey = -1;

        private const int WH_KEYBOARD_LL = 13;                    //Type of Hook - Low Level Keyboard
        private const int WM_KEYDOWN = 0x0100;                    //Value passed on KeyDown
        private const int WM_SYSKEYDOWN = 0x0104;
        private const int WM_SYSKEYUP = 0x0105;
        private const int WM_KEYUP = 0x0101;                      //Value passed on KeyUp
        private static LowLevelKeyboardProc _proc = HookCallback; //The function called when a key is pressed
        private static IntPtr _hookID = IntPtr.Zero;
        private static int CurrentModifiers = 0;                  // Bitmask of currently pressed modifiers

        private static List<Hotkey> registeredHotkeys;

        public static MonitorMode CurrentMonitorMode = MonitorMode.Monitor;


        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);



        public void Init()
        {
            _hookID = SetHook(_proc);  //Set our hook

        }

        /// <summary>
        /// Register a hotkey combination to be monitored for
        /// </summary>
        /// <param name="modifiers"></param>
        /// <param name="hotkey"></param>
        public void RegisterHotkey(int[] modifiers, int hotkey, Action onInvoke)
        {
            
            Hotkey h = new Hotkey()
            {
                Modifiers = modifiers,
                HotkeyCode = hotkey,
                OnInvoke = onInvoke
            };
            if (registeredHotkeys == null)
                registeredHotkeys = new List<Hotkey>();
            registeredHotkeys.Add(h);
        }

        public void DeregisterHotkey()
        {
            /* cleanup methods... TODO */
        }

        public void RegisterHotkey(HotkeyData hotkeyData, Action onInvoke)
        {
            Hotkey h = new Hotkey()
            {
                Modifiers = hotkeyData.GetModifiers(),
                HotkeyCode = hotkeyData.GetHotkey(),
                OnInvoke = onInvoke
            };
            if (registeredHotkeys == null)
                registeredHotkeys = new List<Hotkey>();
            registeredHotkeys.Add(h);
        }

        /// <summary>
        /// Wipes all current hotkeydata
        /// </summary>
        public void WipeHotkeys()
        {
            if(registeredHotkeys != null)
                registeredHotkeys.Clear();
        }

        ~HotkeyManager()
        {
            UnhookWindowsHookEx(_hookID);
        }

        /// <summary>
        /// Hooks the OS keyboad and registers a delegate to be called by the os on keyboard events
        /// </summary>
        /// <param name="proc"></param>
        /// <returns></returns>
        private static IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                    GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        public const int BIT_SHIFT_CONSTANT_OFFSET = 0xa0;// + 1;

        public static bool AnyKeyPressed()
        {
            return (!(CurrentModifiers == 0 && OtherPressedKey == -1));
        }
 

        /// <summary>
        /// Returns a string that corresponds to the currently held modifiers + one other key
        /// </summary>
        /// <returns></returns>
        public static HotkeyData GetCurrentlyHeldHotkeyData()
        {
            List<string> results = new List<string>();
            List<int> intResults = new List<int>();
            int t = CurrentModifiers & (1 << WM_ALT - BIT_SHIFT_CONSTANT_OFFSET);
            if (t > 0)
            {
                results.Add("ALT");
                intResults.Add(WM_ALT);
            }
            t = CurrentModifiers & (1 << WM_CTRL - BIT_SHIFT_CONSTANT_OFFSET);
            if (t > 0)
            {
                results.Add("CTRL");
                intResults.Add(WM_CTRL);

            }

            t = CurrentModifiers & (1 << WM_SHIFT - BIT_SHIFT_CONSTANT_OFFSET);
            if (t > 0)
            {
                results.Add("SHIFT");
                intResults.Add(WM_SHIFT);

            }

            if (OtherPressedKey != -1)
            {
                string key = ((Keys)OtherPressedKey).ToString();
                results.Add(key);
                intResults.Add(OtherPressedKey);

            }

            string res = "";
            if (results.Count > 0)
            {
                for (int j = 0; j < results.Count - 1; j++)
                {
                    res += results[j];
                    res += " + ";
                }
                res += results[results.Count - 1];
            }
            return new HotkeyData(res,intResults.ToArray());
        }


        /// <summary>
        /// Handles the incoming keyboard events
        /// </summary>
        /// <param name="nCode"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0 && (wParam == (IntPtr)WM_KEYDOWN || wParam == (IntPtr)WM_SYSKEYDOWN))         //A Key was pressed down
            {
                int vkCode = Marshal.ReadInt32(lParam);             //Get the keycode

                if (vkCode >= 0xa0 && vkCode <= 0xa4)               // is one of the modifiers? (alt shift control)
                {
                    CurrentModifiers |= (1 << vkCode - BIT_SHIFT_CONSTANT_OFFSET);   // set current modifier bit           
                }
                else                                                // Not a modifier, check hotkeys!
                {
                    
                    OtherPressedKey = vkCode;
                 
                    if (CurrentMonitorMode == MonitorMode.Monitor)
                    {
                        for (int i = 0; i < registeredHotkeys.Count; i++)
                        {
                            if (vkCode == registeredHotkeys[i].HotkeyCode && CheckModifiers(registeredHotkeys[i].Modifiers))
                            {
                                registeredHotkeys[i].OnInvoke.Invoke();
                            }
                        }
                    }
                }


            }

            else if (nCode >= 0 && (wParam == (IntPtr)WM_KEYUP || wParam == (IntPtr)WM_SYSKEYUP))      //KeyUP
            {
                int vkCode = Marshal.ReadInt32(lParam);             //Get Keycode

                if (vkCode >= 0xa0 && vkCode <= 0xa4)               // is one of the modifiers? (alt shift control)
                {
                    CurrentModifiers &= ~(1 << vkCode - BIT_SHIFT_CONSTANT_OFFSET);                     // unset current modifier bit
                }
                else
                {
                    if(OtherPressedKey == vkCode){
                        OtherPressedKey = -1;
                    }
                }
            }


            return CallNextHookEx(_hookID, nCode, wParam, lParam); //Call the next hook
        }

        /// <summary>
        /// Checks if the provided modifier array satisfies the curren modifiers
        /// </summary>
        /// <param name="modifiers"></param>
        /// <returns></returns>
        public static bool CheckModifiers(int[] modifiers)
        {
            int InputModifiers = 0;
            for (int i = 0; i < modifiers.Length; i++)
            {
                InputModifiers |= (1 << (modifiers[i] - BIT_SHIFT_CONSTANT_OFFSET));
            }
          if(InputModifiers == CurrentModifiers)
                    return true;
            return false;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook,
        LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

    }
}
