﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Specialized;


namespace AImageTool
{


    /// paramater : 
    /// Uri -- https://allions.net/angularimg/upload.php
    /// byte[] -- File
    /// string -- Filename
    /// string -- Email
    /// string -- Password (SHA1)
    /// string -- internal-API-Key

    public class ServerConnector
    {
        public delegate void UploadCallback(string response);
        public delegate void ProgressCallback(float uploadProgress);
        public delegate void PrivelegeCallback(int privilegeLevel);

        public void UploadBytesToServer(Uri uri, byte[] file, string filename, string API_KEY, string email, string password, UploadCallback callback, ProgressCallback progressCallback = null)
        {

            Dictionary<string, object> postParams = new Dictionary<string, object>();

            // no need to hash pw anymore, pw will come hashed into this method
            string shApiKey = StringToSHA1(API_KEY);
            password = ServerConnector.StringToSHA1(password);
            postParams.Add("email", email);
            postParams.Add("password", password);
            postParams.Add("apikey", shApiKey);

            postParams.Add("file", new FileParameter(file, filename, "image/png"));

            Debug.WriteLine("FileSize: {0} kb", file.Length / 1024f);

            MultipartFormDataPost(ProgramCore.CurUserSettings.Uri, postParams, callback, progressCallback);

        }

        private static byte[] GetMultipartFormData(Dictionary<string, object> postParameters, string boundary)
        {

            Stream formDataStream = new System.IO.MemoryStream();
            Encoding encoding = UTF8Encoding.UTF8;
            bool needsCLRF = false;
            try
            {
                foreach (var param in postParameters)
                {
                    // Thanks to feedback from commenters, add a CRLF to allow multiple parameters to be added. 
                    // Skip it on the first parameter, add it to subsequent parameters. 
                    if (needsCLRF)
                        formDataStream.Write(encoding.GetBytes("\r\n"), 0, encoding.GetByteCount("\r\n"));

                    needsCLRF = true;

                    if (param.Value is FileParameter)
                    {
                        FileParameter fileToUpload = (FileParameter)param.Value;

                        // Add just the first part of this param, since we will write the file data directly to the Stream 
                        string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n",
                            boundary,
                            param.Key,
                            fileToUpload.FileName ?? param.Key,
                            fileToUpload.ContentType ?? "application/octet-stream");

                        formDataStream.Write(encoding.GetBytes(header), 0, encoding.GetByteCount(header));

                        // Write the file data directly to the Stream, rather than serializing it to a string. 
                        formDataStream.Write(fileToUpload.File, 0, fileToUpload.File.Length);
                    }
                    else
                    {
                        string postData = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
                            boundary,
                            param.Key,
                            param.Value);
                        formDataStream.Write(encoding.GetBytes(postData), 0, encoding.GetByteCount(postData));
                    }
                }

                // Add the end of the request.  Start with a newline 
                string footer = "\r\n--" + boundary + "--\r\n";
                formDataStream.Write(encoding.GetBytes(footer), 0, encoding.GetByteCount(footer));
            }
            catch (Exception)
            {
                throw new Exception("Network Issue");
            }
            // Dump the Stream into a byte[] 
            formDataStream.Position = 0;
            byte[] formData = new byte[formDataStream.Length];
            formDataStream.Read(formData, 0, formData.Length);
            formDataStream.Close();
            return formData;
        }

        public void MultipartFormDataPost(string postUrl, Dictionary<string, object> postParameters, UploadCallback callback, ProgressCallback progressCallback)
        {
            string formDataBoundary = String.Format("----------{0:N}", Guid.NewGuid());
            string contentType = "multipart/form-data; boundary=" + formDataBoundary;

            byte[] formData = GetMultipartFormData(postParameters, formDataBoundary);

            // PostForm(postUrl, contentType, formData, callback);
            PostFormAsyncWithResponseAndProgress(postUrl, contentType, formData, callback, progressCallback);
        }


        public byte[] MultipartFormData(string postUrl, Dictionary<string, object> postParameters)
        {
            string formDataBoundary = String.Format("----------{0:N}", Guid.NewGuid());
            string contentType = "multipart/form-data; boundary=" + formDataBoundary;

            byte[] formData = GetMultipartFormData(postParameters, formDataBoundary);

            return formData;
        }


        private void PostForm(string postUrl, string contentType, byte[] formData, UploadCallback callback)
        {
            Debug.WriteLine("Posting Web Request.");
            HttpWebRequest httpWebRequest = WebRequest.Create(postUrl) as HttpWebRequest;
            httpWebRequest.Proxy = null;
            if (httpWebRequest == null)
            {
                throw new NullReferenceException("request is not a http request");
            }

            // Set up the request properties. 
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = contentType;
            httpWebRequest.CookieContainer = new CookieContainer();
            httpWebRequest.ContentLength = formData.Length;
            Debug.WriteLine("Begin Get Request Stream.");
            httpWebRequest.BeginGetRequestStream((result) =>
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)result.AsyncState;
                    using (Stream requestStream = request.EndGetRequestStream(result))
                    {
                        requestStream.Write(formData, 0, formData.Length);
                        requestStream.Close();
                    }
                    Debug.WriteLine("Got Request Stream, write form and wait for Response.");
                    request.BeginGetResponse(a =>
                    {
                        try
                        {
                            var response = request.EndGetResponse(a);
                            var responseStream = response.GetResponseStream();
                            using (var sr = new StreamReader(responseStream))
                            {
                                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                                {
                                    string responseString = streamReader.ReadToEnd();

                                    if (!string.IsNullOrEmpty(responseString))
                                    {
                                        callback(responseString);
                                    }
                                    else
                                    {
                                        callback("failed: String null");
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            callback("failed: "+ ex.Message);
                        }
                    }, null);
                }
                catch (Exception ex)
                {

                    callback("failed: " + ex.Message);
                }
            }, httpWebRequest);

            //isImageUpload = false;
        }

        private void PostFormAsyncWithResponse(string postUrl, string contentType, byte[] formData, UploadCallback callback)
        {
            HttpWebRequest httpWebRequest = WebRequest.Create(postUrl) as HttpWebRequest;

            if (httpWebRequest == null)
            {
                throw new NullReferenceException("request is not a http request");
            }

            // Set up the request properties. 
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = contentType;
            httpWebRequest.CookieContainer = new CookieContainer();
            httpWebRequest.ContentLength = formData.Length;
            httpWebRequest.BeginGetRequestStream((result) =>
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)result.AsyncState;
                    using (Stream requestStream = request.EndGetRequestStream(result))
                    {
                        requestStream.Write(formData, 0, formData.Length);
                        requestStream.Close();
                    }
                    request.BeginGetResponse(a =>
                    {
                        Debug.WriteLine(a.IsCompleted);
                        try
                        {
                            var response = request.EndGetResponse(a);
                            long contentLength = response.ContentLength;
                            var responseStream = response.GetResponseStream();
                      
                            using (var sr = new StreamReader(responseStream))
                            {
                                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                                {
                                    string responseString = streamReader.ReadToEnd();

                                    if (!string.IsNullOrEmpty(responseString))
                                    {
                                        callback(responseString);
                                    }
                                    else
                                    {
                                        callback("failed: String null");
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            callback("failed: " + ex.Message);
                        }
                    }, null);
                }
                catch (Exception ex)
                {

                    callback("failed: " + ex.Message);
                }
            }, httpWebRequest);

            //isImageUpload = false;
        }

        private void PostFormAsyncWithResponseAndProgress(string postUrl, string contentType, byte[] formData, UploadCallback callback, ProgressCallback progressCallback)
        {
            HttpWebRequest httpWebRequest = WebRequest.Create(postUrl) as HttpWebRequest;

            if (httpWebRequest == null)
            {
                throw new NullReferenceException("request is not a http request");
            }

            // Set up the request properties. 
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = contentType;
            httpWebRequest.CookieContainer = new CookieContainer();
            httpWebRequest.ContentLength = formData.Length;

            byte[] tempBuffer;

            using (Stream reqStream = httpWebRequest.GetRequestStream())
            {
             
                int bytesWritten = 0;
                int ChunkSize = 4096;
                
                while (bytesWritten < formData.Length)
                {
                    int bytesToWrite = formData.Length - bytesWritten < ChunkSize ? formData.Length - bytesWritten : ChunkSize;
                    tempBuffer = new byte[bytesToWrite];

                    Array.Copy(formData, bytesWritten, tempBuffer, 0, bytesToWrite);
                    reqStream.Write(tempBuffer, 0, bytesToWrite);
                    progressCallback(bytesWritten / (float)formData.Length);
                    bytesWritten += bytesToWrite;
                }

                using (WebResponse response = httpWebRequest.GetResponse())
                {
                    var responseStream = response.GetResponseStream();

                    using (var sr = new StreamReader(responseStream))
                    {
                        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                        {
                            string responseString = streamReader.ReadToEnd();

                            if (!string.IsNullOrEmpty(responseString))
                            {
                                callback(responseString);
                            }
                            else
                            {
                                callback("failed: String null");
                            }

                        }
                    }
                }
            }
        }

        private void WriteToStream(Stream s, string txt)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(txt);
            s.Write(bytes, 0, bytes.Length);
        }

        private void WriteToStream(Stream s, byte[] bytes)
        {
            s.Write(bytes, 0, bytes.Length);
        }

        public static string StringToSHA1(string str)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                byte[] hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(str));
                StringBuilder sb = new StringBuilder(hash.Length * 2);
                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("x2"));
                }
                return sb.ToString();
            }
        }

        public void GetUserPrivilegesFromServerAsync(string postUrl, string password, string email, PrivelegeCallback callback)
        {
            postUrl += "/login";

            Dictionary<string, object> postParams = new Dictionary<string, object>();

            password = ServerConnector.StringToSHA1(password);
            postParams.Add("email", email);
            postParams.Add("password", password);

            byte[] data = MultipartFormData(postUrl, postParams);

            string formDataBoundary = String.Format("----------{0:N}", Guid.NewGuid());
            string contentType = "multipart/form-data; boundary=" + formDataBoundary;



            PostForm(postUrl, contentType, data, (s) =>
               {
                   callback(Int32.Parse(s));
               });
            }
        
       
    }

    public class FileParameter
    {
        public byte[] File { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public FileParameter(byte[] file) : this(file, null) { }
        public FileParameter(byte[] file, string filename) : this(file, filename, null) { }
        public FileParameter(byte[] file, string filename, string contenttype)
        {
            File = file;
            FileName = filename;
            ContentType = contenttype;
        }
    }
}
