﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AImageTool
{
    [Serializable]
    public class HotkeyData
    {
        public string Name;
        public int[] Hotkeys;

        public HotkeyData(string item1, int[] item2)
        {
            Name = item1;
            Hotkeys = item2;
        }

        public int[] GetModifiers()
        {
            List<int> modifiers = new List<int>();
            for (int i = 0; i < Hotkeys.Length; i++)
            {
                if (Hotkeys[i] == HotkeyManager.WM_ALT || Hotkeys[i] == HotkeyManager.WM_CTRL || Hotkeys[i] == HotkeyManager.WM_SHIFT)
                    modifiers.Add(Hotkeys[i]);
                
            }
            return modifiers.ToArray();
        }
        public int GetHotkey()
        {
            if (Hotkeys.Length > 0)
                return Hotkeys[Hotkeys.Length - 1];
            else
                return -1;
        }
    }

    /// <summary>
    /// Defines the capture mode for fullscreen capturing
    /// </summary>
    public enum FullScreenCaptureMode
    {
        All,
        Primary,
        Mouse
    }

    /// <summary>
    /// Defines the strategy for upload notifications and monitoring
    /// </summary>
    public enum NotificationMode
    {
        // Works badly on Win10
        WindowsNotifications,
        ProgressBars
    }
    

    /// <summary>
    /// Contains all settings that can be made by the user and manages serialization and deserialization
    /// </summary>
    [Serializable]
    public class UserSettings
    {
        // Int for serialization that translates non serializable ImageFormat to a serializable int with encapsulation
        private int imageFormatInternal;
        // Defines whether the Images should be saved to the harddrive after screenshotting
        private bool saveToHDD;
        // Defines the default path images should be saved to
        private string defaultSavePath;
        // Defines whether the images should be uploaded to the server after screenshotting 
        private bool uploadToServer;
        // Defines whether the link should be copied to the clipboard after the upload is complete
        private bool copyToClipboard;
        // The url that should be used to upload to
        private string uri;
        // The email that should be used when signing in
        private string email;
        // The password that should be used
        private string password;
        // The api key that should be used
        private string apikey;
        // Should the programm be started with windows?
        private bool startOnStartup;
        // Defines the current capturemode for fullscreen capture
        private FullScreenCaptureMode fullCaptureMode;
        // Defines whether the images should be postprocessed for artifact removal
        private bool postProcessArtifactRemoval;
        // Defines whether ShareDat will start in the tray or with the settings window open
        private bool startInTray;
        // YOU CANT SEE IT!
        private bool johnCena = true;
        // Defines whether ShareDat will use a custom sound upon finishing an operation
        private bool useCustomShareSound;
        // Path to the custom sound wav file
        private string customShareSoundPath;
        // The desired Quality for WebM Recording
        private int webMQuality = 1;
        // The desired FrameRate for WebMRecording
        private int webMFramerate = 25;
        // Defines whether the mouse cursor should be included in Webm Recordings 
        private bool webMrecCursor;
        // Defines the Strategy for for upload notifications and monitoring
        NotificationMode notificationMode; 

        // Defines Hotkeysettings for the various actions
        private HotkeyData hotkeySettings_FullScreen;
        private HotkeyData hotkeySettings_AreaScreen;
        private HotkeyData hotkeySettings_UploadClipBoard;
        private HotkeyData hotkeySettings_WebmRecord;
        private HotkeyData hotkeySettings_WebmRecordStop;



        // Encapsulation
        public ImageFormat ImgFormat
        {
            get
            {
                if (imageFormatInternal == 1)
                {
                    return ImageFormat.Jpeg;
                }
                else if (imageFormatInternal == 2)
                {
                    return ImageFormat.Png;
                }
                else if (imageFormatInternal == 3)
                {
                    return ImageFormat.Gif;
                }
                else if (imageFormatInternal == 4)
                {
                    return ImageFormat.Tiff;
                }
                else
                {
                    return ImageFormat.Jpeg;
                }
            }

            set
            {
                // set internal
                if(value == ImageFormat.Jpeg)
                {
                    imageFormatInternal = 1;
                }
                else if (value == ImageFormat.Png)
                {
                    imageFormatInternal = 2;
                }
                else if (value == ImageFormat.Gif)
                {
                    imageFormatInternal = 3;
                }
                else if (value == ImageFormat.Tiff)
                {
                    imageFormatInternal = 4;
                }

                
            }
        }

        public bool SaveToHDD
        {
            get
            {
                return saveToHDD;
            }

            set
            {
                saveToHDD = value;

            }
        }

        public string DefaultSavePath
        {
            get
            {
                return defaultSavePath;
            }

            set
            {
                defaultSavePath = value;

            }
        }

        public bool UploadToServer
        {
            get
            {
                return uploadToServer;
            }

            set
            {
                uploadToServer = value;

            }
        }

        public bool CopyToClipboard
        {
            get
            {
                return copyToClipboard;
            }

            set
            {
                copyToClipboard = value;

            }
        }

        public int ImageFormatInternal
        {
            get
            {
                return imageFormatInternal;
            }

            set
            {
                imageFormatInternal = value;

            }
        }

        public FullScreenCaptureMode FullCaptureMode
        {
            get
            {
                return fullCaptureMode;
            }

            set
            {
                fullCaptureMode = value;

            }
        }

        public string Uri
        {
            get
            {
                return uri;
            }

            set
            {
                uri = value;

            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;

            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;

            }
        }

        public string Apikey
        {
            get
            {
                return apikey;
            }

            set
            {
                apikey = value;

            }
        }

        public bool StartOnStartup
        {
            get
            {
                return startOnStartup;
            }

            set
            {
                startOnStartup = value;
                Save(this);
                if(value == true && !CheckIsRegisteredStartup() )
                {
                    RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                        rk.SetValue("ShareDat", Application.ExecutablePath.ToString());
                    
                }
                else if(value==false && CheckIsRegisteredStartup() )
                {
                    RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                    rk.DeleteValue("ShareDat", false);
                }
            }
        }

        public bool PostProcessArtifactRemoval
        {
            get
            {
                return postProcessArtifactRemoval;
            }

            set
            {
                postProcessArtifactRemoval = value;
 
            }
        }

        public bool StartInTray
        {
            get
            {
                return startInTray;
            }

            set
            {
                startInTray = value;
            }
        }

        public bool JohnCena
        {
            get
            {
                return johnCena;
            }

            set
            {
                johnCena = value;
            }
        }

        public bool UseCustomShareSound
        {
            get
            {
                return useCustomShareSound;
            }

            set
            {
                useCustomShareSound = value;
            }
        }

        public string CustomShareSoundPath
        {
            get
            {
                return customShareSoundPath;
            }

            set
            {
                customShareSoundPath = value;
            }
        }

        public int WebMQuality
        {
            get
            {
                return webMQuality;
            }

            set
            {
                webMQuality = value;
            }
        }

        public int WebMFramerate
        {
            get
            {
                return webMFramerate;
            }

            set
            {
                webMFramerate = value;
            }
        }

        public bool WebMrecCursor
        {
            get
            {
                return webMrecCursor;
            }

            set
            {
                webMrecCursor = value;
            }
        }

        public HotkeyData HotkeySettings_FullScreen
        {
            get
            {
                if (hotkeySettings_FullScreen == null)
                {
                    hotkeySettings_FullScreen = new HotkeyData("CTRL + SHIFT + D1",
                        new int[] { HotkeyManager.WM_CTRL, HotkeyManager.WM_SHIFT, (int)Keys.D1 });
                }
                return hotkeySettings_FullScreen;
            }

            set
            {
                hotkeySettings_FullScreen = value;
            }
        }

        public HotkeyData HotkeySettings_AreaScreen
        {
            get
            {
                if (hotkeySettings_AreaScreen == null)
                {
                    hotkeySettings_AreaScreen = new HotkeyData("CTRL + SHIFT + D4",
                        new int[] { HotkeyManager.WM_CTRL, HotkeyManager.WM_SHIFT, (int)Keys.D4 });
                }
                return hotkeySettings_AreaScreen;
            }

            set
            {
                hotkeySettings_AreaScreen = value;
            }
        }

        public HotkeyData HotkeySettings_UploadClipBoard
        {
            get
            {
                if (hotkeySettings_UploadClipBoard == null)
                {
                    hotkeySettings_UploadClipBoard = new HotkeyData("CTRL + SHIFT + D5",
                        new int[] { HotkeyManager.WM_CTRL, HotkeyManager.WM_SHIFT, (int)Keys.D5 });
                }
                return hotkeySettings_UploadClipBoard;
            }

            set
            {
                hotkeySettings_UploadClipBoard = value;
            }
        }

        public HotkeyData HotkeySettings_WebmRecord
        {
            get
            {
                if (hotkeySettings_WebmRecord == null)
                {
                    hotkeySettings_WebmRecord = new HotkeyData("CTRL + SHIFT + D2",
                        new int[] { HotkeyManager.WM_CTRL, HotkeyManager.WM_SHIFT, (int)Keys.D2 });
                }
                return hotkeySettings_WebmRecord;
            }

            set
            {
                hotkeySettings_WebmRecord = value;
            }
        }

        public HotkeyData HotkeySettings_WebmRecordStop
        {
            get
            {
                if(hotkeySettings_WebmRecordStop == null)
                {
                    hotkeySettings_WebmRecordStop = new HotkeyData("CTRL + SHIFT + D3",
                        new int[] { HotkeyManager.WM_CTRL, HotkeyManager.WM_SHIFT, (int)Keys.D3 });
                }
                return hotkeySettings_WebmRecordStop;
            }

            set
            {
                hotkeySettings_WebmRecordStop = value;
            }
        }

        public NotificationMode NotificationMode
        {
            get
            {
                return notificationMode;
            }

            set
            {
                notificationMode = value;
            }
        }


        /// <summary>
        /// Checks if the Programm has been registered in the registry for startup on boot
        /// </summary>
        /// <returns></returns>
        public bool CheckIsRegisteredStartup()
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            return (!(rk.GetValue("ShareDat") == null));
        }

        /// <summary>
        /// Saves the given settings to a settings.dat file in the root directory of the programm
        /// </summary>
        /// <param name="settings"></param>
        public static void Save(UserSettings settings)
        {
            string appdataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            if (!Directory.Exists(appdataFolder + "/ShareDat")){
                Directory.CreateDirectory(appdataFolder + "/ShareDat");
            }
            FileStream fs = new FileStream(appdataFolder+"/ShareDat/usersettings.dat", FileMode.Create, FileAccess.Write);// System.Security.Permissions.FileIOPermissionAccess.Write);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, settings);

            fs.Close();
        }

        /// <summary>
        /// Loads the current "settings.dat"
        /// if usersettings do not exist yet, or are corrupt returns a default user setting file
        /// </summary>
        /// <returns></returns>
        public static UserSettings Load()
        {
            UserSettings settings;
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = null;


            try {
                string appdataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                if (!Directory.Exists(appdataFolder + "/ShareDat")){
                    Directory.CreateDirectory(appdataFolder + "/ShareDat");
                }

                fs = new FileStream(appdataFolder + "/ShareDat/usersettings.dat", FileMode.Open, FileAccess.Read);
                settings = (UserSettings)bf.Deserialize(fs);
            }
            // couldn't load or convert, probably file corruption or wrong format or file is not there yet
            catch{
                settings = CreateDefaultUserSettings();
            }

            if(fs!=null)
                fs.Close();

            return settings;
        }

        /// <summary>
        /// returns a default usersettings struct
        /// </summary>
        /// <returns></returns>
        public static UserSettings CreateDefaultUserSettings()
        {
            return new UserSettings()
            {
                ImgFormat = ImageFormat.Jpeg,
                DefaultSavePath = "/Screenshots/",
                UploadToServer = true,
                CopyToClipboard = true,
                SaveToHDD = false,
                WebMFramerate = 25,
                WebMQuality = 7
            };
        }
    }
}
