﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Screna;
using Screna.Avi;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using NReco.VideoConverter;

namespace AImageTool { 


    public class VideoHandler
    {
        public delegate void ProgressCallback(object sender, ConvertProgressEventArgs args);

        private IImageProvider m_provider;
        private Recorder m_recorder;
        private IVideoFileWriter m_writer;

        private string filePath;


        public VideoHandler()
        {
            
        }

        public bool isRecording()
        {
            if (m_recorder == null)
                return false;
            return m_recorder.State == RecorderState.Recording;
        }

        public void StartRecording(Rectangle rect)
        {

            IImageProvider screen = new ScreenProvider(Screen.PrimaryScreen);
            // caputure an area that is a wee bit smaller so we dont capture the borders
            rect.Location = new Point(rect.Location.X + 1,rect.Location.Y+1);
            rect.Size = new Size(rect.Size.Width - 2, rect.Size.Height - 2);

            if (ProgramCore.CurUserSettings.WebMrecCursor)
                m_provider = new RegionProvider(rect, new MouseCursor());
            else
                m_provider = new RegionProvider(rect);

            string appdataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            if (!Directory.Exists(appdataFolder + "/ShareDat"))
            {
                Directory.CreateDirectory(appdataFolder + "/ShareDat");
            }
            filePath = appdataFolder + "/ShareDat/" + Path.GetRandomFileName() + ".avi";

            AviCodec codec = AviCodec.MotionJpeg;

            codec.Quality = ProgramCore.CurUserSettings.WebMQuality * 10;

            m_writer = new AviWriter(filePath,codec);

            m_recorder = new Recorder(m_writer, m_provider, ProgramCore.CurUserSettings.WebMFramerate);
            m_recorder.Start();

        }

        public string ConvertToWebM(string aviPath, ProgressCallback progCallback = null)
        {
            FFMpegConverter converter = new FFMpegConverter();
            string webmPath = aviPath.Substring(0, aviPath.Length - 4) + ".webm";

            converter.ConvertProgress += new EventHandler<ConvertProgressEventArgs>(progCallback);
            converter.ConvertMedia(aviPath,webmPath, "webm");
            
            System.IO.File.Delete(aviPath);
            return webmPath;
        }

        /// <summary>
        /// Stops the recording and returns the path of the temporary file
        /// </summary>
        /// <returns></returns>
        public string StopRecording()
        {
            m_recorder.Stop();
            return filePath;
        }


    }
}
