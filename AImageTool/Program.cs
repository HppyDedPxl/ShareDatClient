﻿//#define BUILD
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace AImageTool
{
    static class Program
    {
        public static int VersionNumber = 160924;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
#if BUILD
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(MyHandler);
            
            try
            {
                
#endif
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainGUI mgui = new MainGUI();
            Application.Run(mgui);

            
#if BUILD


            }
            catch (Exception e)
            {


                //string fileName2 = "CrashException.txt";

                //using (StreamWriter fs = new StreamWriter(fileName2))
                //{

                //    fs.Write(e.Message);
                //    fs.Close();
                //    Console.WriteLine("MyHandler caught : " + e.Message);
                //}

                //string fileName = Path.Combine("MiniDumpDemo_Mainline.mdmp");

                //using (FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.Write))
                //{

                //    MiniDump.Write(fs.SafeFileHandle, MiniDump.Option.WithFullMemory);
                //    fs.Close();

                //}

                MessageBox.Show("ShareDat Crashed! The program has created a dump file inside the .exe's directory. \nPlease give this to your neighbourhood ShareDat dev!"  + e.Message, "Cheesy Crashes! NOO!   \n", MessageBoxButtons.OK);

         

            }
        }

        static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            string fileName = "CrashException.txt";

            //using (StreamWriter fs = new StreamWriter(fileName))
            //{
            //    Exception e = (Exception)args.ExceptionObject;
            //    fs.Write(e.Message);
            //    fs.Close();
            //    Console.WriteLine("MyHandler caught : " + e.Message);
            //}
#endif
        }
        
    }
}
