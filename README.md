# ShareDat_Client

A Desktop image and video sharing application originally developed for ShareDat (https://sharedat.allions.net).

Currently Supports:

- Fullscreen Capture
- Partial Screen Capture
- MultiScreen Support
- WebM Recording
- Automated HDD saves
- Automated Uploads to angular image services

