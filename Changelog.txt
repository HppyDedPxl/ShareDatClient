## a.160924 ##
	- Fixed several Crashes
	- Added several Error handling windows for faulty respones from the server and settings
	- Added a new notification system, Progress Bars!
	- You can't see him... :'( He's gone
	- Prepared backend to recieve user privileges


## a.160612 ##
	Yay, a big update!
	- ShareDat now supports Recording WebM files!
	- Customizable Hotkeys
	
## a.160522 ##
    - ShareDat no longer crashes when sharing a zero text area
    - ShareDat partial screen capture can now be canceled with escape
    - ShareDat can now Share temp image clipboard content (e.g. photoshop layers)
    - ShareDat now support custom confirmation sounds
    - smthfunny.exe

## a.160515 ##
    - Added support for Clipboard sharing (strg+shift+3)
	Text will be shared as txt
	single file will be shared as is
	multiple files will be zipped and shared as the zip
    - Changed fileextensions to lowercase
    - The Icon now animates during uploading


## a.160514 ##
    - Added auto update functionality

## a.160505 ##
    - Added a splashscreen that now shows upon programm start for 3 seconds
    - Changed image naming to img_MMdd_HHss_ms from img_MMddHHmmss_guid

## a.160504 ##
    - Removed the old UI Code from the project
    - Restructured how internal data is handled
    - Fixed some bugs with the new UI
    - Fixed some error messages appearing for no actual reason
    - Fixed resaving when a password was entered causing the password to be corrupted
    - ShareDat does not longer appear in the alt+tab reel

## a.160503 ##
    - Another UI with a menu from tray to get into a master UI window

## a.160502 ##
    - Implemented new UI (Still testing all functionality! Report bugs ASAP!)
    - Changed the Usersettings storage to AppData, should resolve access right
        exception that caused ShareDat to crash with windows boot
    - Fixed windows boot crash. Probably.

## a.160501 ##
    - Limited background threads execution to 10 times a second
    - Added the Screena Library in preperation for video Capture features
